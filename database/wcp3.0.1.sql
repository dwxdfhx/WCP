/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50155
Source Host           : 127.0.0.1:3399
Source Database       : wcp2

Target Server Type    : MYSQL
Target Server Version : 50155
File Encoding         : 65001

Date: 2015-11-09 19:55:54
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `alone_applog`
-- ----------------------------
DROP TABLE IF EXISTS `alone_applog`;
CREATE TABLE `alone_applog` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `DESCRIBES` varchar(1024) NOT NULL,
  `APPUSER` varchar(32) NOT NULL,
  `LEVELS` varchar(32) DEFAULT NULL,
  `METHOD` varchar(128) DEFAULT NULL,
  `CLASSNAME` varchar(128) DEFAULT NULL,
  `IP` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_Reference_9` (`APPUSER`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of alone_applog
-- ----------------------------
INSERT INTO `alone_applog` VALUES ('402888ac506e722201506e7228ec0000', '20151016101946', '知识共享平台--系统准备运行2项', 'NONE', 'INFO', 'init', 'com.farm.web.task.SysInit', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac506e722201506e7229140001', '20151016101946', '注册配置文件config.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac506e722201506e72291a0002', '20151016101946', '注册配置文件indexConfig.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac506e722201506e7229200003', '20151016101946', '注册配置文件jdbc.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac506e722201506e7229270004', '20151016101946', '注册配置文件document.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac506e722201506e72292e0005', '20151016101946', '注册配置文件cache.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac506e722201506e7229340006', '20151016101946', '注册配置文件webapp.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac506e722201506e72293b0007', '20151016101946', '注册配置文件i18n.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac506e722201506e7229e80008', '20151016101947', 'started \'任务调度\'', 'NONE', 'INFO', 'execute', 'com.farm.quartz.adapter.StartSysTask', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac506e766d01506e7673960000', '20151016102428', '知识共享平台--系统准备运行2项', 'NONE', 'INFO', 'init', 'com.farm.web.task.SysInit', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac506e766d01506e7673bd0001', '20151016102428', '注册配置文件config.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac506e766d01506e7673c10002', '20151016102428', '注册配置文件indexConfig.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac506e766d01506e7673c80003', '20151016102428', '注册配置文件jdbc.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac506e766d01506e7673ce0004', '20151016102428', '注册配置文件document.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac506e766d01506e7673d50005', '20151016102428', '注册配置文件cache.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac506e766d01506e7673db0006', '20151016102428', '注册配置文件webapp.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac506e766d01506e7673e10007', '20151016102428', '注册配置文件i18n.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac506e766d01506e76747f0008', '20151016102428', 'started \'任务调度\'', 'NONE', 'INFO', 'execute', 'com.farm.quartz.adapter.StartSysTask', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac506e766d01506e76c2460009', '20151016102448', 'EXCUTE-IQL:WHERE(TITLE,TEXT=关于JAVA中URL传递中文参数，取值是乱码的解决办法) at [c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\file, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\site]', 'NONE', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac506e766d01506e76c36a000a', '20151016102448', '关闭索引，并建立硬盘索引:c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\file', 'NONE', 'DEBUG', 'close', 'com.farm.lucene.server.DocIndexImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac506e766d01506e76c3b6000b', '20151016102448', '关闭索引，并建立硬盘索引:c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know', 'NONE', 'DEBUG', 'close', 'com.farm.lucene.server.DocIndexImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac506e766d01506e76c3e9000c', '20151016102448', '关闭索引，并建立硬盘索引:c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\site', 'NONE', 'DEBUG', 'close', 'com.farm.lucene.server.DocIndexImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac506e766d01506e76c438000d', '20151016102448', '共检索到记录0条，用时496毫秒', 'NONE', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac506e7d9401506e7d9ac20000', '20151016103216', '知识共享平台--系统准备运行2项', 'NONE', 'INFO', 'init', 'com.farm.web.task.SysInit', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac506e7d9401506e7d9ae80001', '20151016103216', '注册配置文件config.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac506e7d9401506e7d9aed0002', '20151016103216', '注册配置文件indexConfig.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac506e7d9401506e7d9af40003', '20151016103216', '注册配置文件jdbc.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac506e7d9401506e7d9afa0004', '20151016103216', '注册配置文件document.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac506e7d9401506e7d9b010005', '20151016103216', '注册配置文件cache.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac506e7d9401506e7d9b070006', '20151016103216', '注册配置文件webapp.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac506e7d9401506e7d9b0d0007', '20151016103216', '注册配置文件i18n.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac506e7d9401506e7d9ba70008', '20151016103217', 'started \'任务调度\'', 'NONE', 'INFO', 'execute', 'com.farm.quartz.adapter.StartSysTask', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac506e7d9401506e867e500012', '20151016104159', 'No mapping found for HTTP request with URI [/wcp/AloneParameter_All_navigatPage.do] in DispatcherServlet with name \'spring\'', '40288b854a329988014a329a12f30002', 'WARN', 'noHandlerFound', 'org.springframework.web.servlet.DispatcherServlet', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac506e7d9401506e86a1e10013', '20151016104208', 'No mapping found for HTTP request with URI [/wcp/AloneParameter_User_navigatPage.do] in DispatcherServlet with name \'spring\'', '40288b854a329988014a329a12f30002', 'WARN', 'noHandlerFound', 'org.springframework.web.servlet.DispatcherServlet', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac506e7d9401506e88facc0014', '20151016104442', 'No mapping found for HTTP request with URI [/wcp/AloneParameter_All_navigatPage.do] in DispatcherServlet with name \'spring\'', '40288b854a329988014a329a12f30002', 'WARN', 'noHandlerFound', 'org.springframework.web.servlet.DispatcherServlet', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac506e7d9401506e8905d40015', '20151016104445', 'No mapping found for HTTP request with URI [/wcp/AloneParameter_All_navigatPage.do] in DispatcherServlet with name \'spring\'', '40288b854a329988014a329a12f30002', 'WARN', 'noHandlerFound', 'org.springframework.web.servlet.DispatcherServlet', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac506eaf5b01506eaf61ce0000', '20151016112639', '知识共享平台--系统准备运行2项', 'NONE', 'INFO', 'init', 'com.farm.web.task.SysInit', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac506eaf5b01506eaf61f70001', '20151016112639', '注册配置文件config.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac506eaf5b01506eaf61fd0002', '20151016112639', '注册配置文件indexConfig.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac506eaf5b01506eaf62030003', '20151016112639', '注册配置文件jdbc.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac506eaf5b01506eaf620a0004', '20151016112639', '注册配置文件document.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac506eaf5b01506eaf62120005', '20151016112639', '注册配置文件cache.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac506eaf5b01506eaf62190006', '20151016112639', '注册配置文件webapp.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac506eaf5b01506eaf62200007', '20151016112639', '注册配置文件i18n.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac506eaf5b01506eaf62e10008', '20151016112639', 'started \'任务调度\'', 'NONE', 'INFO', 'execute', 'com.farm.quartz.adapter.StartSysTask', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac506eaf5b01506ebfe84f000d', '20151016114442', '关闭索引，并建立硬盘索引:c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know', '40288b854a329988014a329a12f30002', 'DEBUG', 'close', 'com.farm.lucene.server.DocIndexImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac506eaf5b01506ec1c8320011', '20151016114644', '删除磁盘索引ID:402888ac506eaf5b01506ebfe7aa000b,total time: 6 ms', '40288b854a329988014a329a12f30002', 'DEBUG', 'deleteFhysicsIndex', 'com.farm.lucene.server.DocIndexImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac506eaf5b01506ec1c8400012', '20151016114644', '关闭索引，并建立硬盘索引:c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know', '40288b854a329988014a329a12f30002', 'DEBUG', 'close', 'com.farm.lucene.server.DocIndexImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac506eaf5b01506ec1c86a0013', '20151016114644', 'java.lang.NullPointerException: value cannot be null', '40288b854a329988014a329a12f30002', 'ERROR', 'editSubmit', 'com.farm.doc.controller.ActionFarmDocQuery', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac506eaf5b01506ec206e20015', '20151016114700', '删除磁盘索引ID:402888ac506eaf5b01506ebfe7aa000b,total time: 1 ms', '40288b854a329988014a329a12f30002', 'DEBUG', 'deleteFhysicsIndex', 'com.farm.lucene.server.DocIndexImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac506eaf5b01506ec206e90016', '20151016114700', '关闭索引，并建立硬盘索引:c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know', '40288b854a329988014a329a12f30002', 'DEBUG', 'close', 'com.farm.lucene.server.DocIndexImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac506eaf5b01506ec207270017', '20151016114701', 'java.lang.NullPointerException: value cannot be null', '40288b854a329988014a329a12f30002', 'ERROR', 'editSubmit', 'com.farm.doc.controller.ActionFarmDocQuery', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac506eaf5b01506ec23a290019', '20151016114714', '删除磁盘索引ID:402888ac506eaf5b01506ebfe7aa000b,total time: 2 ms', '40288b854a329988014a329a12f30002', 'DEBUG', 'deleteFhysicsIndex', 'com.farm.lucene.server.DocIndexImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac506eaf5b01506ec23a2f001a', '20151016114714', '关闭索引，并建立硬盘索引:c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know', '40288b854a329988014a329a12f30002', 'DEBUG', 'close', 'com.farm.lucene.server.DocIndexImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac506eaf5b01506ec23a61001b', '20151016114714', 'java.lang.NullPointerException: value cannot be null', '40288b854a329988014a329a12f30002', 'ERROR', 'editSubmit', 'com.farm.doc.controller.ActionFarmDocQuery', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac506eaf5b01506f29b045001c', '20151016134014', 'EXCUTE-IQL:WHERE(TITLE,TEXT=WCP3系统简介) at [c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\file, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\site]', 'NONE', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac506eaf5b01506f29b089001d', '20151016134014', '共检索到记录0条，用时67毫秒', 'NONE', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac506f5b7401506f5b7afe0000', '20151016143437', '知识共享平台--系统准备运行2项', 'NONE', 'INFO', 'init', 'com.farm.web.task.SysInit', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac506f5b7401506f5b7b260001', '20151016143437', '注册配置文件config.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac506f5b7401506f5b7b2c0002', '20151016143437', '注册配置文件indexConfig.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac506f5b7401506f5b7b380003', '20151016143437', '注册配置文件jdbc.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac506f5b7401506f5b7b3f0004', '20151016143437', '注册配置文件document.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac506f5b7401506f5b7b460005', '20151016143437', '注册配置文件cache.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac506f5b7401506f5b7b4d0006', '20151016143437', '注册配置文件webapp.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac506f5b7401506f5b7b530007', '20151016143437', '注册配置文件i18n.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac506f5b7401506f5b7bed0008', '20151016143437', 'started \'任务调度\'', 'NONE', 'INFO', 'execute', 'com.farm.quartz.adapter.StartSysTask', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac506f5b7401506f5c3eb2000b', '20151016143527', '索引引擎建立内存索引:AUTHOR:系统管理员|TYPENAME:|VISITNUM:0|TEXT:本项目的应用场景是管理业务团队的相关知识（API、代码片段、知识定义、技术经验...）但是其应用并不|DOCDESCRIBE:本项目的应用场景是管理业务团队的相关知识（API、代码片段、知识定义、技术经验...）但是其应用并不|ID:402888ac506eaf5b01506ebfe7aa000b|DOMTYPE:1|PUBTIME:2015-10-16 14:35:12|USERID:40288b854a329988014a329a12f30002|TITLE:WCP3系统简介|TAGKEY:,知识,好评,权限,管理,用户,小组,文档,x ,发布,使用|', '40288b854a329988014a329a12f30002', 'DEBUG', 'indexDoc', 'com.farm.lucene.server.DocIndexImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac506f5b7401506f5c3ec9000c', '20151016143527', '关闭索引，并建立硬盘索引:c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know', '40288b854a329988014a329a12f30002', 'DEBUG', 'close', 'com.farm.lucene.server.DocIndexImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac506f5b7401506f5c77ac000d', '20151016143542', 'EXCUTE-IQL:WHERE(TITLE,TEXT=WCP3系统简介) at [c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\file, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\site]', '40288b854a329988014a329a12f30002', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac506f5b7401506f5c77d2000e', '20151016143542', '关闭索引，并建立硬盘索引:c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\file', '40288b854a329988014a329a12f30002', 'DEBUG', 'close', 'com.farm.lucene.server.DocIndexImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac506f5b7401506f5c782e000f', '20151016143542', '关闭索引，并建立硬盘索引:c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\site', '40288b854a329988014a329a12f30002', 'DEBUG', 'close', 'com.farm.lucene.server.DocIndexImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac506f5b7401506f5c78910010', '20151016143542', '共检索到记录1条，用时229毫秒', '40288b854a329988014a329a12f30002', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac506f5b7401506f5e33490011', '20151016143735', 'EXCUTE-IQL:WHERE(TITLE,TEXT=WCP3系统简介) at [c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\file, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\site]', '40288b854a329988014a329a12f30002', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac506f5b7401506f5e33550012', '20151016143735', '共检索到记录1条，用时12毫秒', '40288b854a329988014a329a12f30002', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac506f5b7401506f5f9d820014', '20151016143908', '删除磁盘索引ID:402888ac506eaf5b01506ebfe7aa000b,total time: 86 ms', '40288b854a329988014a329a12f30002', 'DEBUG', 'deleteFhysicsIndex', 'com.farm.lucene.server.DocIndexImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac506f5b7401506f5f9d930015', '20151016143908', '索引引擎建立内存索引:AUTHOR:系统管理员|TYPENAME:WCP帮助|VISITNUM:0|TEXT:本项目的应用场景是管理业务团队的相关知识（API、代码片段、知识定义、技术经验...）但是其应用并不|DOCDESCRIBE:本项目的应用场景是管理业务团队的相关知识（API、代码片段、知识定义、技术经验...）但是其应用并不|ID:402888ac506eaf5b01506ebfe7aa000b|DOMTYPE:1|PUBTIME:20151016143512|USERID:40288b854a329988014a329a12f30002|TITLE:WCP3系统简介|TAGKEY:,知识,好评,权限,管理,用户,小组,文档,x ,发布,使用|', '40288b854a329988014a329a12f30002', 'DEBUG', 'indexDoc', 'com.farm.lucene.server.DocIndexImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac506f5b7401506f5f9daa0016', '20151016143908', '关闭索引，并建立硬盘索引:c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know', '40288b854a329988014a329a12f30002', 'DEBUG', 'close', 'com.farm.lucene.server.DocIndexImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac506f5b7401506f5f9ed80017', '20151016143909', 'EXCUTE-IQL:WHERE(TITLE,TEXT=WCP3系统简介) at [c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\file, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\site]', '40288b854a329988014a329a12f30002', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac506f5b7401506f5f9ee30018', '20151016143909', '共检索到记录1条，用时11毫秒', '40288b854a329988014a329a12f30002', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac506f5b7401506f8931d8001a', '20151016152433', '删除磁盘索引ID:402888ac506eaf5b01506ebfe7aa000b,total time: 91 ms', '40288b854a329988014a329a12f30002', 'DEBUG', 'deleteFhysicsIndex', 'com.farm.lucene.server.DocIndexImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac506f5b7401506f8931ea001b', '20151016152433', '索引引擎建立内存索引:AUTHOR:系统管理员|TYPENAME:WCP帮助|VISITNUM:0|TEXT:本项目的应用场景是管理业务团队的相关知识（API、代码片段、知识定义、技术经验...）但是其应用并不|DOCDESCRIBE:本项目的应用场景是管理业务团队的相关知识（API、代码片段、知识定义、技术经验...）但是其应用并不|ID:402888ac506eaf5b01506ebfe7aa000b|DOMTYPE:1|PUBTIME:20151016143512|USERID:40288b854a329988014a329a12f30002|TITLE:WCP3系统简介|TAGKEY:,知识,好评,权限,管理,用户,小组,文档,x ,发布,使用|', '40288b854a329988014a329a12f30002', 'DEBUG', 'indexDoc', 'com.farm.lucene.server.DocIndexImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac506f5b7401506f89320c001c', '20151016152433', '关闭索引，并建立硬盘索引:c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know', '40288b854a329988014a329a12f30002', 'DEBUG', 'close', 'com.farm.lucene.server.DocIndexImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac506f5b7401506f893359001d', '20151016152434', 'EXCUTE-IQL:WHERE(TITLE,TEXT=WCP3系统简介) at [c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\file, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\site]', '40288b854a329988014a329a12f30002', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac506f5b7401506f893366001e', '20151016152434', '共检索到记录1条，用时12毫秒', '40288b854a329988014a329a12f30002', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac506f5b7401506f8fb8980023', '20151016153141', '索引引擎建立内存索引:AUTHOR:系统管理员|TYPENAME:|VISITNUM:0|TEXT:&nbsp;&nbsp;&nbsp;&nbsp;WCP当前版本（开源版）允许任何人进行自由下载和自由|DOCDESCRIBE:&nbsp;&nbsp;&nbsp;&nbsp;WCP当前版本（开源版）允许任何人进行自由下载和自由|ID:402888ac506f5b7401506f8fb8860021|DOMTYPE:1|PUBTIME:20151016153141|USERID:40288b854a329988014a329a12f30002|TITLE:licence|TAGKEY:,使用,系统,过程中,可能,可能会,使用过程中,测试,数据,过程,任何|', '40288b854a329988014a329a12f30002', 'DEBUG', 'indexDoc', 'com.farm.lucene.server.DocIndexImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac506f5b7401506f8fb8ba0024', '20151016153141', '关闭索引，并建立硬盘索引:c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know', '40288b854a329988014a329a12f30002', 'DEBUG', 'close', 'com.farm.lucene.server.DocIndexImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac506f5b7401506f8fd0dd0025', '20151016153147', 'EXCUTE-IQL:WHERE(TITLE,TEXT=licence) at [c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\file, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\site]', '40288b854a329988014a329a12f30002', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac506f5b7401506f8fd0ef0026', '20151016153147', '共检索到记录1条，用时20毫秒', '40288b854a329988014a329a12f30002', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac506f5b7401506f952460002e', '20151016153736', 'EXCUTE-IQL:WHERE(TITLE,TEXT,TAGKEY,TYPENAME,AUTHOR=wcp) at [c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\site, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\file]', '40288b854a329988014a329a12f30002', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac506f5b7401506f95246f002f', '20151016153736', '共检索到记录2条，用时14毫秒', '40288b854a329988014a329a12f30002', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac506f5b7401506f9724f00042', '20151016153947', '索引引擎建立内存索引:AUTHOR:系统管理员|TYPENAME:|VISITNUM:0|TEXT:知识管理知识管理是企业管理的一项重要内容，主流商业管理课程如EMBA、及MBA等均将“知识管理”作为|DOCDESCRIBE:知识管理知识管理是企业管理的一项重要内容，主流商业管理课程如EMBA、及MBA等均将“知识管理”作为|ID:402888ac506f5b7401506f9724c70040|DOMTYPE:1|PUBTIME:20151016153947|USERID:40288b854a329988014a329a12f30002|TITLE:知识管理|TAGKEY:管理,知识,企业,项目,组织,内容,进行,系统,目标,实施|', '40288b854a329988014a329a12f30002', 'DEBUG', 'indexDoc', 'com.farm.lucene.server.DocIndexImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac506f5b7401506f97251e0043', '20151016153947', '关闭索引，并建立硬盘索引:c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know', '40288b854a329988014a329a12f30002', 'DEBUG', 'close', 'com.farm.lucene.server.DocIndexImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac506f5b7401506f97855e0044', '20151016154012', 'EXCUTE-IQL:WHERE(TITLE,TEXT=知识管理) at [c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\file, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\site]', '40288b854a329988014a329a12f30002', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac506f5b7401506f9785a50045', '20151016154012', '共检索到记录2条，用时73毫秒', '40288b854a329988014a329a12f30002', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac506f5b7401506f9842dd0046', '20151016154101', 'EXCUTE-IQL:WHERE(TITLE,TEXT=WCP3系统简介) at [c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\file, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\site]', '40288b854a329988014a329a12f30002', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac506f5b7401506f9842f60047', '20151016154101', '共检索到记录3条，用时25毫秒', '40288b854a329988014a329a12f30002', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac506f5b7401506fbc2610004a', '20151016162012', 'EXCUTE-IQL:WHERE(TITLE,TEXT=WCP3系统简介) at [c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\file, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\site]', 'NONE', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '192.168.8.243');
INSERT INTO `alone_applog` VALUES ('402888ac506f5b7401506fbc264a004b', '20151016162013', '共检索到记录3条，用时61毫秒', 'NONE', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '192.168.8.243');
INSERT INTO `alone_applog` VALUES ('402888ac506f5b7401506fbc50e7004c', '20151016162023', 'EXCUTE-IQL:WHERE(TITLE,TEXT=WCP3系统简介) at [c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\file, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\site]', 'NONE', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '192.168.8.243');
INSERT INTO `alone_applog` VALUES ('402888ac506f5b7401506fbc510c004d', '20151016162023', '共检索到记录3条，用时40毫秒', 'NONE', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '192.168.8.243');
INSERT INTO `alone_applog` VALUES ('402888ac506f5b7401507011adeb004e', '20151016175338', 'EXCUTE-IQL:WHERE(TITLE,TEXT=WCP3系统简介) at [c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\file, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\site]', 'NONE', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac506f5b7401507011ae04004f', '20151016175338', '共检索到记录3条，用时27毫秒', 'NONE', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac506f5b7401507011cb240050', '20151016175345', 'EXCUTE-IQL:WHERE(AUTHOR=系统管理员) at [c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\site, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\file]', 'NONE', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac506f5b7401507011cb2c0051', '20151016175345', '共检索到记录3条，用时7毫秒', 'NONE', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac506f5b740150701674020052', '20151016175851', 'EXCUTE-IQL:WHERE(TITLE,TEXT=WCP3系统简介) at [c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\file, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\site]', 'NONE', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac506f5b740150701674130053', '20151016175851', '共检索到记录3条，用时16毫秒', 'NONE', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac506f5b740150701972760054', '20151016180207', 'EXCUTE-IQL:WHERE(TITLE,TEXT=WCP3系统简介) at [c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\file, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\site]', '40288b854a329988014a329a12f30002', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac506f5b740150701972850055', '20151016180207', '共检索到记录3条，用时15毫秒', '40288b854a329988014a329a12f30002', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac506f5b740150701996880056', '20151016180216', 'EXCUTE-IQL:WHERE(TITLE,TEXT=licence) at [c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\file, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\site]', '40288b854a329988014a329a12f30002', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac506f5b740150701996900057', '20151016180216', '共检索到记录1条，用时7毫秒', '40288b854a329988014a329a12f30002', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac50702014015070201ae60000', '20151016180923', '知识共享平台--系统准备运行2项', 'NONE', 'INFO', 'init', 'com.farm.web.task.SysInit', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac50702014015070201b0e0001', '20151016180923', '注册配置文件config.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac50702014015070201b130002', '20151016180923', '注册配置文件indexConfig.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac50702014015070201b1a0003', '20151016180923', '注册配置文件jdbc.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac50702014015070201b210004', '20151016180923', '注册配置文件document.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac50702014015070201b280005', '20151016180923', '注册配置文件cache.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac50702014015070201b2f0006', '20151016180923', '注册配置文件webapp.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac50702014015070201b360007', '20151016180923', '注册配置文件i18n.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac50702014015070201bd30008', '20151016180923', 'started \'任务调度\'', 'NONE', 'INFO', 'execute', 'com.farm.quartz.adapter.StartSysTask', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac5070201401507020467e0009', '20151016180934', 'EXCUTE-IQL:WHERE(TITLE,TEXT=WCP3系统简介) at [c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\file, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\site]', '40288b854a329988014a329a12f30002', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac507020140150702047d0000a', '20151016180935', '关闭索引，并建立硬盘索引:c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\file', '40288b854a329988014a329a12f30002', 'DEBUG', 'close', 'com.farm.lucene.server.DocIndexImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac5070201401507020480c000b', '20151016180935', '关闭索引，并建立硬盘索引:c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know', '40288b854a329988014a329a12f30002', 'DEBUG', 'close', 'com.farm.lucene.server.DocIndexImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac50702014015070204845000c', '20151016180935', '关闭索引，并建立硬盘索引:c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\site', '40288b854a329988014a329a12f30002', 'DEBUG', 'close', 'com.farm.lucene.server.DocIndexImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac50702014015070204894000d', '20151016180935', '共检索到记录0条，用时531毫秒', '40288b854a329988014a329a12f30002', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac5070201401507020f7c40010', '20151016181020', '删除磁盘索引ID:402888ac506eaf5b01506ebfe7aa000b,total time: 2 ms', '40288b854a329988014a329a12f30002', 'DEBUG', 'deleteFhysicsIndex', 'com.farm.lucene.server.DocIndexImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac5070201401507020f7e70011', '20151016181020', '索引引擎建立内存索引:AUTHOR:系统管理员|TYPENAME:|VISITNUM:0|TEXT:本项目的应用场景是管理业务团队的相关知识（API、代码片段、知识定义、技术经验...）但是其应用并不|DOCDESCRIBE:本项目的应用场景是管理业务团队的相关知识（API、代码片段、知识定义、技术经验...）但是其应用并不|ID:402888ac506eaf5b01506ebfe7aa000b|DOMTYPE:1|PUBTIME:2015-10-16 18:09:57|USERID:40288b854a329988014a329a12f30002|TITLE:WCP3系统简介|TAGKEY:,知识,好评,权限,管理,用户,小组,文档,x ,发布,使用|', '40288b854a329988014a329a12f30002', 'DEBUG', 'indexDoc', 'com.farm.lucene.server.DocIndexImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac5070201401507020f7fb0012', '20151016181020', '关闭索引，并建立硬盘索引:c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know', '40288b854a329988014a329a12f30002', 'DEBUG', 'close', 'com.farm.lucene.server.DocIndexImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac507020140150702110740013', '20151016181026', 'EXCUTE-IQL:WHERE(TITLE,TEXT=WCP3系统简介) at [c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\file, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\site]', '40288b854a329988014a329a12f30002', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac507020140150702110a80014', '20151016181026', '共检索到记录1条，用时52毫秒', '40288b854a329988014a329a12f30002', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac507020140150702131d20015', '20151016181035', 'EXCUTE-IQL:WHERE(TITLE,TEXT=licence) at [c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\file, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\site]', '40288b854a329988014a329a12f30002', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac507020140150702131d80016', '20151016181035', '共检索到记录0条，用时6毫秒', '40288b854a329988014a329a12f30002', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac5070307901507030801c0000', '20151016182718', '知识共享平台--系统准备运行2项', 'NONE', 'INFO', 'init', 'com.farm.web.task.SysInit', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150703080430001', '20151016182718', '注册配置文件config.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150703080480002', '20151016182718', '注册配置文件indexConfig.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac5070307901507030804f0003', '20151016182718', '注册配置文件jdbc.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150703080550004', '20151016182718', '注册配置文件document.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac5070307901507030805c0005', '20151016182718', '注册配置文件cache.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150703080620006', '20151016182718', '注册配置文件webapp.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150703080680007', '20151016182718', '注册配置文件i18n.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac5070307901507030811a0008', '20151016182718', 'started \'任务调度\'', 'NONE', 'INFO', 'execute', 'com.farm.quartz.adapter.StartSysTask', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac5070307901507030b8210009', '20151016182732', 'EXCUTE-IQL:WHERE(TITLE,TEXT=WCP3系统简介) at [c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\file, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\site]', 'NONE', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac5070307901507030b95f000a', '20151016182732', '共检索到记录1条，用时318毫秒', 'NONE', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac5070307901507030c677000b', '20151016182736', 'EXCUTE-IQL:WHERE(TAGKEY=小组) at [c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\site, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\file]', 'NONE', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac5070307901507030c67f000c', '20151016182736', '共检索到记录1条，用时8毫秒', 'NONE', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac5070307901507030cd3a000d', '20151016182737', 'EXCUTE-IQL:WHERE(TITLE,TEXT=WCP3系统简介) at [c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\file, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\site]', 'NONE', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac5070307901507030cd47000e', '20151016182737', '共检索到记录1条，用时13毫秒', 'NONE', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150704be762000f', '20151016185714', 'EXCUTE-IQL:WHERE(TITLE,TEXT=WCP3系统简介) at [c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\file, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\site]', 'NONE', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150704be7710010', '20151016185714', '共检索到记录1条，用时15毫秒', 'NONE', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150704c047a0011', '20151016185721', 'EXCUTE-IQL:WHERE(TITLE,TEXT=WCP3系统简介) at [c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\file, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\site]', 'NONE', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150704c048c0012', '20151016185721', '共检索到记录1条，用时18毫秒', 'NONE', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150704c126f0013', '20151016185725', 'EXCUTE-IQL:WHERE(TITLE,TEXT=licence) at [c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\file, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\site]', 'NONE', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150704c12750014', '20151016185725', '共检索到记录0条，用时6毫秒', 'NONE', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150704eef590015', '20151016190032', 'EXCUTE-IQL:WHERE(TITLE,TEXT,TAGKEY,TYPENAME,AUTHOR=licence) at [c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\site, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\file]', 'NONE', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150704eef610016', '20151016190032', '共检索到记录0条，用时9毫秒', 'NONE', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150704ef78a0017', '20151016190034', 'EXCUTE-IQL:WHERE(TITLE,TEXT,TAGKEY,TYPENAME,AUTHOR=java) at [c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\site, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\file]', 'NONE', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150704ef7930018', '20151016190034', '共检索到记录0条，用时9毫秒', 'NONE', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150704ef7970019', '20151016190034', 'EXCUTE-IQL:WHERE(TITLE,TEXT,TAGKEY,TYPENAME,AUTHOR=小组) at [c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\site, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\file]', 'NONE', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150704ef7a2001a', '20151016190034', '共检索到记录1条，用时11毫秒', 'NONE', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150704f21dd001b', '20151016190045', 'EXCUTE-IQL:WHERE(TITLE,TEXT,TAGKEY,TYPENAME,AUTHOR=WCP当前) at [c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\site, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\file]', 'NONE', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150704f21ea001c', '20151016190045', '共检索到记录1条，用时13毫秒', 'NONE', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150704f310c001d', '20151016190049', 'EXCUTE-IQL:WHERE(TITLE,TEXT,TAGKEY,TYPENAME,AUTHOR=WCP) at [c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\site, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\file]', 'NONE', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150704f3117001e', '20151016190049', '共检索到记录1条，用时11毫秒', 'NONE', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150704f37a7001f', '20151016190051', 'EXCUTE-IQL:WHERE(TITLE,TEXT,TAGKEY,TYPENAME,AUTHOR=java) at [c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\site, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\file]', 'NONE', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150704f37b00020', '20151016190051', '共检索到记录0条，用时8毫秒', 'NONE', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150704f37b70021', '20151016190051', 'EXCUTE-IQL:WHERE(TITLE,TEXT,TAGKEY,TYPENAME,AUTHOR=WCP当前) at [c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\site, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\file]', 'NONE', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150704f37c30022', '20151016190051', '共检索到记录1条，用时11毫秒', 'NONE', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150704f3c610023', '20151016190052', 'EXCUTE-IQL:WHERE(TITLE,TEXT,TAGKEY,TYPENAME,AUTHOR=java) at [c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\site, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\file]', 'NONE', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150704f3c680024', '20151016190052', 'EXCUTE-IQL:WHERE(TITLE,TEXT,TAGKEY,TYPENAME,AUTHOR=licence) at [c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\site, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\file]', 'NONE', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150704f3c6c0025', '20151016190052', '共检索到记录0条，用时8毫秒', 'NONE', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150704f3c700026', '20151016190052', '共检索到记录0条，用时6毫秒', 'NONE', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150704f48060027', '20151016190055', 'EXCUTE-IQL:WHERE(TITLE,TEXT=licence) at [c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\file, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\site]', 'NONE', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150704f48100028', '20151016190055', '共检索到记录0条，用时11毫秒', 'NONE', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150704f70f90029', '20151016190105', 'EXCUTE-IQL:WHERE(TITLE,TEXT=licence) at [c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\file, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\site]', '40288b854a329988014a329a12f30002', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150704f7101002a', '20151016190105', '共检索到记录0条，用时7毫秒', '40288b854a329988014a329a12f30002', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150704faf03002c', '20151016190121', '删除磁盘索引ID:402888ac506f5b7401506f8fb8860021,total time: 4 ms', '40288b854a329988014a329a12f30002', 'DEBUG', 'deleteFhysicsIndex', 'com.farm.lucene.server.DocIndexImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150704faf1a002d', '20151016190121', '索引引擎建立内存索引:AUTHOR:系统管理员|TYPENAME:WCP帮助|VISITNUM:0|TEXT:&nbsp;&nbsp;&nbsp;&nbsp;WCP当前版本（开源版）允许任何人进行自由下载和自由|DOCDESCRIBE:&nbsp;&nbsp;&nbsp;&nbsp;WCP当前版本（开源版）允许任何人进行自由下载和自由|ID:402888ac506f5b7401506f8fb8860021|DOMTYPE:1|PUBTIME:20151016153141|USERID:40288b854a329988014a329a12f30002|TITLE:licence|TAGKEY:,使用,系统,过程中,可能,可能会,使用过程中,测试,数据,过程,任何|', '40288b854a329988014a329a12f30002', 'DEBUG', 'indexDoc', 'com.farm.lucene.server.DocIndexImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150704faf35002e', '20151016190121', '关闭索引，并建立硬盘索引:c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know', '40288b854a329988014a329a12f30002', 'DEBUG', 'close', 'com.farm.lucene.server.DocIndexImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150704fb050002f', '20151016190122', 'EXCUTE-IQL:WHERE(TITLE,TEXT=licence) at [c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\file, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\site]', '40288b854a329988014a329a12f30002', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150704fb05b0030', '20151016190122', '共检索到记录1条，用时11毫秒', '40288b854a329988014a329a12f30002', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150704fbf010031', '20151016190125', 'EXCUTE-IQL:WHERE(TITLE,TEXT=licence) at [c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\file, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\site]', '40288b854a329988014a329a12f30002', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150704fbf0a0032', '20151016190125', '共检索到记录1条，用时8毫秒', '40288b854a329988014a329a12f30002', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150704fd2980033', '20151016190130', 'EXCUTE-IQL:WHERE(TITLE,TEXT,TAGKEY,TYPENAME,AUTHOR=java) at [c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\site, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\file]', '40288b854a329988014a329a12f30002', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150704fd2a00034', '20151016190130', '共检索到记录0条，用时8毫秒', '40288b854a329988014a329a12f30002', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150704fd2a60035', '20151016190130', 'EXCUTE-IQL:WHERE(TITLE,TEXT,TAGKEY,TYPENAME,AUTHOR=java) at [c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\site, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\file]', '40288b854a329988014a329a12f30002', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150704fd2ae0036', '20151016190130', '共检索到记录0条，用时7毫秒', '40288b854a329988014a329a12f30002', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150704fd72e0037', '20151016190132', 'EXCUTE-IQL:WHERE(TITLE,TEXT,TAGKEY,TYPENAME,AUTHOR=java) at [c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\site, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\file]', '40288b854a329988014a329a12f30002', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150704fd7350038', '20151016190132', '共检索到记录0条，用时8毫秒', '40288b854a329988014a329a12f30002', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150704fd7450039', '20151016190132', 'EXCUTE-IQL:WHERE(TITLE,TEXT,TAGKEY,TYPENAME,AUTHOR=licence) at [c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\site, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\file]', '40288b854a329988014a329a12f30002', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150704fd74e003a', '20151016190132', '共检索到记录1条，用时8毫秒', '40288b854a329988014a329a12f30002', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150704fdd11003b', '20151016190133', 'EXCUTE-IQL:WHERE(TITLE,TEXT,TAGKEY,TYPENAME,AUTHOR=java) at [c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\site, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\file]', '40288b854a329988014a329a12f30002', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150704fdd19003c', '20151016190133', '共检索到记录0条，用时7毫秒', '40288b854a329988014a329a12f30002', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150704fdd1e003d', '20151016190133', 'EXCUTE-IQL:WHERE(TITLE,TEXT,TAGKEY,TYPENAME,AUTHOR=小组) at [c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\site, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\file]', '40288b854a329988014a329a12f30002', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150704fdd27003e', '20151016190133', '共检索到记录1条，用时9毫秒', '40288b854a329988014a329a12f30002', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150704fe0c4003f', '20151016190134', 'EXCUTE-IQL:WHERE(TITLE,TEXT,TAGKEY,TYPENAME,AUTHOR=java) at [c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\site, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\file]', '40288b854a329988014a329a12f30002', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150704fe0cb0040', '20151016190134', '共检索到记录0条，用时8毫秒', '40288b854a329988014a329a12f30002', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150704fe0d60041', '20151016190134', 'EXCUTE-IQL:WHERE(TITLE,TEXT,TAGKEY,TYPENAME,AUTHOR=WCP当前) at [c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\site, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\file]', '40288b854a329988014a329a12f30002', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150704fe0e00042', '20151016190134', '共检索到记录2条，用时10毫秒', '40288b854a329988014a329a12f30002', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150704fe8020043', '20151016190136', 'EXCUTE-IQL:WHERE(TITLE,TEXT=WCP3系统简介) at [c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\file, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\site]', '40288b854a329988014a329a12f30002', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150704fe8110044', '20151016190136', '共检索到记录2条，用时14毫秒', '40288b854a329988014a329a12f30002', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac5070307901507051cebe0045', '20151016190340', 'EXCUTE-IQL:WHERE(TITLE,TEXT=licence) at [c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\file, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\site]', '40288b854a329988014a329a12f30002', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac5070307901507051cec70046', '20151016190340', '共检索到记录1条，用时8毫秒', '40288b854a329988014a329a12f30002', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150705cbed20047', '20151016191537', 'EXCUTE-IQL:WHERE(TITLE,TEXT=WCP3系统简介) at [c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\file, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\site]', '40288b854a329988014a329a12f30002', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150705cbee00048', '20151016191537', '共检索到记录2条，用时13毫秒', '40288b854a329988014a329a12f30002', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150705cf40d0049', '20151016191551', 'EXCUTE-IQL:WHERE(TITLE,TEXT=WCP3系统简介) at [c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\file, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\site]', '40288b854a329988014a329a12f30002', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150705cf416004a', '20151016191551', '共检索到记录2条，用时10毫秒', '40288b854a329988014a329a12f30002', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150705cf8fa004b', '20151016191552', 'EXCUTE-IQL:WHERE(AUTHOR=系统管理员) at [c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\site, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\file]', '40288b854a329988014a329a12f30002', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150705cf901004c', '20151016191552', '共检索到记录2条，用时7毫秒', '40288b854a329988014a329a12f30002', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150708b91a0004d', '20151016200646', 'EXCUTE-IQL:WHERE(TITLE,TEXT=WCP3系统简介) at [c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\file, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\site]', 'NONE', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '192.168.8.243');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150708b91b3004e', '20151016200646', '共检索到记录2条，用时18毫秒', 'NONE', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '192.168.8.243');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150708bbac3004f', '20151016200656', 'EXCUTE-IQL:WHERE(TITLE,TEXT=WCP3系统简介) at [c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\file, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\know, c:\\wcp3server\\server\\webapps\\wcp\\indexfiles\\site]', 'NONE', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '192.168.8.243');
INSERT INTO `alone_applog` VALUES ('402888ac507030790150708bbacd0050', '20151016200656', '共检索到记录2条，用时10毫秒', 'NONE', 'DEBUG', 'queryByMultiIndex', 'com.farm.lucene.server.DocQueryImpl', '192.168.8.243');
INSERT INTO `alone_applog` VALUES ('402888ac50895df60150895dfd700000', '20151021154729', '知识共享平台--系统准备运行2项', 'NONE', 'INFO', 'init', 'com.farm.web.task.SysInit', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac50895df60150895dfdbc0001', '20151021154729', '注册配置文件config.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac50895df60150895dfdc20002', '20151021154729', '注册配置文件indexConfig.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac50895df60150895dfdc80003', '20151021154729', '注册配置文件jdbc.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac50895df60150895dfdcf0004', '20151021154729', '注册配置文件document.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac50895df60150895dfdd60005', '20151021154729', '注册配置文件cache.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac50895df60150895dfddd0006', '20151021154729', '注册配置文件webapp.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac50895df60150895dfde40007', '20151021154729', '注册配置文件i18n.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac50895df60150895dfebf0008', '20151021154730', 'started \'任务调度\'', 'NONE', 'INFO', 'execute', 'com.farm.quartz.adapter.StartSysTask', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac50ec08730150ec087a640000', '20151109193630', '知识共享平台--系统准备运行2项', 'NONE', 'INFO', 'init', 'com.farm.web.task.SysInit', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac50ec08730150ec087ad20001', '20151109193630', '注册配置文件config.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac50ec08730150ec087adf0002', '20151109193630', '注册配置文件indexConfig.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac50ec08730150ec087aee0003', '20151109193630', '注册配置文件jdbc.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac50ec08730150ec087af90004', '20151109193630', '注册配置文件document.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac50ec08730150ec087b050005', '20151109193630', '注册配置文件cache.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac50ec08730150ec087b100006', '20151109193630', '注册配置文件webapp.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac50ec08730150ec087b1b0007', '20151109193630', '注册配置文件i18n.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac50ec08730150ec087bf20008', '20151109193630', 'started \'任务调度\'', 'NONE', 'INFO', 'execute', 'com.farm.quartz.adapter.StartSysTask', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac50ec09e70150ec09ee8e0000', '20151109193805', '知识共享平台--系统准备运行2项', 'NONE', 'INFO', 'init', 'com.farm.web.task.SysInit', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac50ec09e70150ec09eeb90001', '20151109193805', '注册配置文件config.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac50ec09e70150ec09eebf0002', '20151109193805', '注册配置文件indexConfig.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac50ec09e70150ec09eec70003', '20151109193805', '注册配置文件jdbc.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac50ec09e70150ec09eece0004', '20151109193805', '注册配置文件document.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac50ec09e70150ec09eed60005', '20151109193805', '注册配置文件cache.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac50ec09e70150ec09eede0006', '20151109193805', '注册配置文件webapp.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac50ec09e70150ec09eee60007', '20151109193805', '注册配置文件i18n.properties', 'NONE', 'INFO', 'registConstant', 'com.farm.parameter.service.impl.PropertiesFileService', 'NONE');
INSERT INTO `alone_applog` VALUES ('402888ac50ec09e70150ec09efa00008', '20151109193805', 'started \'任务调度\'', 'NONE', 'INFO', 'execute', 'com.farm.quartz.adapter.StartSysTask', 'NONE');

-- ----------------------------
-- Table structure for `alone_auth_action`
-- ----------------------------
DROP TABLE IF EXISTS `alone_auth_action`;
CREATE TABLE `alone_auth_action` (
  `ID` varchar(32) NOT NULL,
  `AUTHKEY` varchar(128) NOT NULL,
  `NAME` varchar(64) NOT NULL,
  `COMMENTS` varchar(128) DEFAULT NULL,
  `CTIME` varchar(14) NOT NULL,
  `UTIME` varchar(14) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `MUSER` varchar(32) NOT NULL,
  `STATE` char(1) NOT NULL,
  `CHECKIS` char(1) NOT NULL,
  `LOGINIS` char(1) NOT NULL COMMENT '默认所有ACTION都要登录',
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='InnoDB free: 12288 kB alone_action';

-- ----------------------------
-- Records of alone_auth_action
-- ----------------------------
INSERT INTO `alone_auth_action` VALUES ('40288b854a38408e014a384de88a0005', 'action/list', '权限管理_权限定义', null, '20141211154357', '20141211154357', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('40288b854a38974f014a38b3a6700017', 'actiontree/list', '权限管理_权限构造', null, '20141211173505', '20141211173505', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('40288b854a38974f014a38b93d0a0022', 'log/list', '系统配置_系统日志', '', '20141211174111', '20150831184926', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('40288b854a38974f014a38ba24f90024', 'parameter/list', '系统配置_参数定义', '', '20141211174210', '20150831185312', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('40288b854a38974f014a38bafb830026', 'parameter/editlist', '系统配置_系统参数', '', '20141211174305', '20151016161420', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('40288b854a38974f014a38bb431b0028', 'parameter/userelist', '系统配置_个性化参数', '', '20141211174324', '20151016161431', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('40288b854a38974f014a38bd26a0002a', 'dictionary/list', '系统配置_数据字典', '', '20141211174527', '20150831185552', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('40288b854a38974f014a38beae79002d', 'user/list', '组织用户管理_用户管理', '', '20141211174708', '20150831185712', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('40288b854a38974f014a38bf10fc002f', 'organization/list', '组织用户管理_组织机构管理', '', '20141211174733', '20150831185753', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('40288b854a3daac8014a3dfe03990005', 'FarmPortalComponentConsole', '门户管理_组件库', null, '20141212181424', '20141212181424', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('40288b854a3daac8014a3dfefcce0007', 'FarmPortalPortaltreeConsole', '门户管理_门户配置', null, '20141212181528', '20141212181528', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('40288b854a3e037e014a3e1d76810003', 'FarmFormsFormcategoryConsole', '动态表单_表单分类', null, '20141212184845', '20141212184845', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('40288b854a3e037e014a3e1e46150005', 'FarmFormsQueryConsole', '动态表单_查询表单配置', null, '20141212184939', '20141212184939', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('40288b854a50d918014a50dbdc460002', 'HhYWConsole', '系统配置_数据库管理', null, '20141216100953', '20141216100953', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('40288b854a50d918014a50dcfa580005', 'FarmQzTrigger_ACTION_CONSOLE', '任务调度_触发器定义', null, '20141216101106', '20141216101106', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('40288b854a50d918014a50ddb7400007', 'FarmQzTask_ACTION_CONSOLE', '任务调度_任务定义', null, '20141216101155', '20141216101155', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('40288b854a50d918014a50def0aa0009', 'FarmQzScheduler_ACTION_CONSOLE', '任务调度_调度实例', null, '20141216101315', '20141216101315', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('402894ca4a9a155d014a9a1790810003', 'docfile/list', '文档管理_附件管理', '', '20141230152723', '20150831185928', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('402894ca4a9a155d014a9a182dbb0005', 'FarmDocgroupUser_ACTION_CONSOLE', '文档管理_工作小组管理', null, '20141230152803', '20141230152803', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('402894ca4a9a155d014a9a1905200007', 'docgroup/list', '文档管理_工作小组管理', '', '20141230152858', '20150831190059', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('402894ca4a9a155d014a9a1a32940009', 'FarmDocmessage_ACTION_CONSOLE', '文档管理_文档留言', null, '20141230153015', '20141230153015', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('402894ca4a9a155d014a9a1a8dd3000b', 'doctype/list', '文档管理_文档分类', '', '20141230153039', '20150831190127', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('402894ca4a9a155d014a9a1afe60000d', 'doc/list', '文档管理_文档管理', '', '20141230153108', '20150901182811', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('40288b854ab4231a014ab45e15850003', 'FarmCodeProject_ACTION_CONSOLE', '代码工程_工程定义', null, '20150104175432', '20150104175432', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('40288b854ab4231a014ab45ee9fb0005', 'FarmCodeGuide_ACTION_CONSOLE', '代码工程_生成向导', null, '20150104175526', '20150104175526', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('402888a84f970dad014f971444550009', 'farmtop/list', '文档管理_置顶文档', '', '20150904143851', '20150904144025', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('402894ca4add11f6014add19da9d000a', 'FarmPlanTaskConsole', '当前任务_计划任务', null, '20150112154426', '20150112154426', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('402894ca4ae0e797014ae1ab12120031', 'docIndex/list', '文档管理_全文索引', '', '20150113130132', '20150831190209', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('402888a850472ac50150472d85070009', 'weburl/list', '文档管理_推荐服务', '', '20151008191936', '20151008191936', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');

-- ----------------------------
-- Table structure for `alone_auth_actiontree`
-- ----------------------------
DROP TABLE IF EXISTS `alone_auth_actiontree`;
CREATE TABLE `alone_auth_actiontree` (
  `ID` varchar(32) NOT NULL,
  `SORT` int(11) NOT NULL,
  `PARENTID` varchar(32) NOT NULL,
  `NAME` varchar(64) NOT NULL,
  `TREECODE` varchar(256) NOT NULL,
  `COMMENTS` varchar(128) DEFAULT NULL,
  `TYPE` char(1) NOT NULL COMMENT '分类、菜单、权限',
  `CTIME` varchar(14) NOT NULL,
  `UTIME` varchar(14) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `UUSER` varchar(32) NOT NULL,
  `STATE` char(1) NOT NULL,
  `ACTIONID` varchar(32) DEFAULT NULL,
  `DOMAIN` varchar(64) NOT NULL,
  `ICON` varchar(64) DEFAULT NULL,
  `IMGID` varchar(32) DEFAULT NULL,
  `PARAMS` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_Reference_7` (`ACTIONID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='InnoDB free: 12288 kB; (`action`) REFER `alone/alone_action`';

-- ----------------------------
-- Records of alone_auth_actiontree
-- ----------------------------
INSERT INTO `alone_auth_actiontree` VALUES ('40288b854a38974f014a38b93d0a0023', '10', '40288b854a38408e014a384c4f3c0002', '系统日志', '40288b854a38408e014a384c4f3c000240288b854a38974f014a38b93d0a0023', '', '2', '20141211174111', '20141211174111', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '40288b854a38974f014a38b93d0a0022', 'alone', 'icon-tip', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('40288b854a38974f014a38b3a6700018', '2', '40288b854a38408e014a384c4f3c0002', '权限构造', '40288b854a38408e014a384c4f3c000240288b854a38974f014a38b3a6700018', '', '2', '20141211173505', '20141211173505', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '40288b854a38974f014a38b3a6700017', 'alone', 'icon-category', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('40288b854a38408e014a384c4f3c0002', '1', 'NONE', '系统配置', '40288b854a38408e014a384c4f3c0002', '', '1', '20141211154212', '20141211173900', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', 'alone', 'icon-sprocket_dark', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('40288b854a38974f014a38bafb830027', '3', '40288b854a38408e014a384c4f3c0002', '系统参数', '40288b854a38408e014a384c4f3c000240288b854a38974f014a38bafb830027', '', '2', '20141211174305', '20141211174305', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '40288b854a38974f014a38bafb830026', 'alone', 'icon-sprocket_dark', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('40288b854a38974f014a38bb431b0029', '4', '40288b854a38408e014a384c4f3c0002', '个性化参数', '40288b854a38408e014a384c4f3c000240288b854a38974f014a38bb431b0029', '', '2', '20141211174324', '20141211174435', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '40288b854a38974f014a38bb431b0028', 'alone', 'icon-client_account_template', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('40288b854a38974f014a38bd26a0002b', '5', '40288b854a38408e014a384c4f3c0002', '数据字典', '40288b854a38408e014a384c4f3c000240288b854a38974f014a38bd26a0002b', '', '2', '20141211174527', '20141211174527', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '40288b854a38974f014a38bd26a0002a', 'alone', 'icon-address-book', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('40288b854a38974f014a38be1805002c', '2', 'NONE', '组织用户管理', '40288b854a38974f014a38be1805002c', '', '1', '20141211174629', '20141211174629', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', null, 'alone', 'icon-group_green_edit', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('40288b854a38974f014a38beae79002e', '1', '40288b854a38974f014a38be1805002c', '用户管理', '40288b854a38974f014a38be1805002c40288b854a38974f014a38beae79002e', '', '2', '20141211174708', '20141211174708', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '40288b854a38974f014a38beae79002d', 'alone', 'icon-hire-me', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('40288b854a38974f014a38bf10fc0030', '2', '40288b854a38974f014a38be1805002c', '组织机构管理', '40288b854a38974f014a38be1805002c40288b854a38974f014a38bf10fc0030', '', '2', '20141211174733', '20141211174733', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '40288b854a38974f014a38bf10fc002f', 'alone', 'icon-customers', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('402888ac506f5b7401506fb899b20049', '1', '40288b854a38408e014a384c4f3c0002', 'URL注册', '40288b854a38408e014a384c4f3c0002402888ac506f5b7401506fb899b20049', '', '2', '20151016161620', '20151016161620', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '40288b854a38974f014a38ba24f90024', 'alone', 'icon-edit', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('402894ca4ae0e797014ae1ab12120032', '6', '402894ca4a9a155d014a9a16561d0002', '全文索引', '402894ca4a9a155d014a9a16561d0002402894ca4ae0e797014ae1ab12120032', '', '2', '20150113130132', '20150113130132', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '402894ca4ae0e797014ae1ab12120031', 'alone', 'icon-application_osx_terminal', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('40288b854a50d918014a50dc82980004', '11', '40288b854a38408e014a384c4f3c0002', '任务调度', '40288b854a38408e014a384c4f3c000240288b854a50d918014a50dc82980004', '', '1', '20141216101036', '20141216101036', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', null, 'alone', 'icon-application_osx_terminal', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('40288b854a50d918014a50def0aa000a', '3', '40288b854a50d918014a50dc82980004', '调度实例', '40288b854a38408e014a384c4f3c000240288b854a50d918014a50dc8298000440288b854a50d918014a50def0aa000a', '', '2', '20141216101315', '20141216101315', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '40288b854a50d918014a50def0aa0009', 'alone', 'icon-future-projects', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('402894ca4a9a155d014a9a16561d0002', '3', 'NONE', '文档管理', '402894ca4a9a155d014a9a16561d0002', '', '1', '20141230152602', '20141230152602', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', null, 'alone', 'icon-archives', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('402894ca4a9a155d014a9a1790810004', '1', '402894ca4a9a155d014a9a16561d0002', '附件管理', '402894ca4a9a155d014a9a16561d0002402894ca4a9a155d014a9a1790810004', '', '2', '20141230152723', '20141230152723', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '402894ca4a9a155d014a9a1790810003', 'alone', 'icon-save', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('402894ca4a9a155d014a9a1905200008', '3', '402894ca4a9a155d014a9a16561d0002', '工作小组管理', '402894ca4a9a155d014a9a16561d0002402894ca4a9a155d014a9a1905200008', '', '2', '20141230152858', '20141230152858', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '402894ca4a9a155d014a9a1905200007', 'alone', 'icon-customers', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('402894ca4a9a155d014a9a1a8de3000c', '5', '402894ca4a9a155d014a9a16561d0002', '文档分类', '402894ca4a9a155d014a9a16561d0002402894ca4a9a155d014a9a1a8de3000c', '', '2', '20141230153039', '20141230153039', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '402894ca4a9a155d014a9a1a8dd3000b', 'alone', 'icon-category', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('402894ca4a9a155d014a9a1afe60000e', '6', '402894ca4a9a155d014a9a16561d0002', '文档管理', '402894ca4a9a155d014a9a16561d0002402894ca4a9a155d014a9a1afe60000e', '', '2', '20141230153107', '20141230153107', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '402894ca4a9a155d014a9a1afe60000d', 'alone', 'icon-blog', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('402888a850472ac50150472e2d14000a', '92', '402894ca4a9a155d014a9a16561d0002', '推荐服务', '402894ca4a9a155d014a9a16561d0002402888a850472ac50150472e2d14000a', '', '2', '20151008192019', '20151008192206', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '402888a850472ac50150472d85070009', 'alone', 'icon-star', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('402888a84f835c36014f835f72cb000e', '1', '40288b854a38408e014a384c4f3c0002', '权限定义', '40288b854a38408e014a384c4f3c0002402888a84f835c36014f835f72cb000e', '', '2', '20150831184834', '20150831184834', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '40288b854a38408e014a384de88a0005', 'alone', 'icon-communication', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('402888a84f970dad014f971545c3000a', '9', '402894ca4a9a155d014a9a16561d0002', '置顶文档', '402894ca4a9a155d014a9a16561d0002402888a84f970dad014f971545c3000a', '', '2', '20150904143957', '20150904144052', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '402888a84f970dad014f971444550009', 'alone', 'icon-upcoming-work', null, '');

-- ----------------------------
-- Table structure for `alone_auth_organization`
-- ----------------------------
DROP TABLE IF EXISTS `alone_auth_organization`;
CREATE TABLE `alone_auth_organization` (
  `ID` varchar(32) NOT NULL,
  `TREECODE` varchar(256) NOT NULL,
  `COMMENTS` varchar(128) DEFAULT NULL,
  `NAME` varchar(64) NOT NULL,
  `CTIME` varchar(14) NOT NULL,
  `UTIME` varchar(14) NOT NULL,
  `STATE` char(1) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `MUSER` varchar(32) NOT NULL,
  `PARENTID` varchar(32) DEFAULT NULL,
  `SORT` int(11) DEFAULT NULL,
  `TYPE` char(1) NOT NULL COMMENT '组织类型：1科室、2班组、3队组、0其他',
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='组织类型：科室、班组、队组、其他';

-- ----------------------------
-- Records of alone_auth_organization
-- ----------------------------
INSERT INTO `alone_auth_organization` VALUES ('402888a84fcb6d88014fcb6f3e90000a', '402888a84fcb6d88014fcb6f3e90000a', '11', '总公司', '20150914183829', '20151016104010', '1', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'NONE', '1', '1');
INSERT INTO `alone_auth_organization` VALUES ('402888a84fcb6d88014fcb6fa14e000c', '402888a84fcb6d88014fcb6f3e90000a402888a84fcb6d88014fcb6fa14e000c', '', '项目部', '20150914183854', '20150914184005', '1', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '402888a84fcb6d88014fcb6f3e90000a', '1', '1');
INSERT INTO `alone_auth_organization` VALUES ('402888a84fcb6d88014fcb6fd5b7000d', '402888a84fcb6d88014fcb6f3e90000a402888a84fcb6d88014fcb6fd5b7000d', '', '商务部', '20150914183908', '20150914183908', '1', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '402888a84fcb6d88014fcb6f3e90000a', '1', '1');
INSERT INTO `alone_auth_organization` VALUES ('402888a84fcb6d88014fcb70eb4d000e', '402888a84fcb6d88014fcb6f3e90000a402888a84fcb6d88014fcb6fa14e000c402888a84fcb6d88014fcb70eb4d000e', '', '研发部', '20150914184019', '20150914184019', '1', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '402888a84fcb6d88014fcb6fa14e000c', '1', '1');
INSERT INTO `alone_auth_organization` VALUES ('402888a84fcb6d88014fcb7101da000f', '402888a84fcb6d88014fcb6f3e90000a402888a84fcb6d88014fcb6fa14e000c402888a84fcb6d88014fcb7101da000f', '', '测试部', '20150914184025', '20150914184025', '1', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '402888a84fcb6d88014fcb6fa14e000c', '1', '1');

-- ----------------------------
-- Table structure for `alone_auth_post`
-- ----------------------------
DROP TABLE IF EXISTS `alone_auth_post`;
CREATE TABLE `alone_auth_post` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `ETIME` varchar(16) NOT NULL,
  `CUSERNAME` varchar(64) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `EUSERNAME` varchar(64) NOT NULL,
  `EUSER` varchar(32) NOT NULL,
  `PSTATE` varchar(2) NOT NULL,
  `ORGANIZATIONID` varchar(32) NOT NULL,
  `NAME` varchar(64) NOT NULL,
  `EXTENDIS` varchar(2) NOT NULL COMMENT '0:否1:是（默认否）',
  PRIMARY KEY (`ID`),
  KEY `FK_Reference_51` (`ORGANIZATIONID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of alone_auth_post
-- ----------------------------
INSERT INTO `alone_auth_post` VALUES ('402888a84fcb6d88014fcb7168010010', '20150914184051', '20150914184116', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '1', '402888a84fcb6d88014fcb70eb4d000e', '项目经理', '1');
INSERT INTO `alone_auth_post` VALUES ('402888a84fcb6d88014fcb7230d40013', '20150914184142', '20150914184142', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '1', '402888a84fcb6d88014fcb7101da000f', '测试经理', '0');
INSERT INTO `alone_auth_post` VALUES ('402888a84fcb6d88014fcb7293220014', '20150914184207', '20150914193437', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '1', '402888a84fcb6d88014fcb6fa14e000c', '项目总监', '1');
INSERT INTO `alone_auth_post` VALUES ('402888a84fcb96ee014fcbaa88250020', '20150914194314', '20150914194314', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '1', '402888a84fcb6d88014fcb6f3e90000a', '默认岗位', '1');

-- ----------------------------
-- Table structure for `alone_auth_postaction`
-- ----------------------------
DROP TABLE IF EXISTS `alone_auth_postaction`;
CREATE TABLE `alone_auth_postaction` (
  `ID` varchar(32) NOT NULL,
  `MENUID` varchar(32) NOT NULL,
  `POSTID` varchar(32) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_Reference_8` (`MENUID`),
  KEY `FK_Reference_9` (`POSTID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='InnoDB free: 12288 kB; (`actionid`) REFER `alone/alone_actio';

-- ----------------------------
-- Records of alone_auth_postaction
-- ----------------------------
INSERT INTO `alone_auth_postaction` VALUES ('402888a84fcb6d88014fcb71ec240012', '402888a84f835c36014f835f72cb000e', '402888a84fcb6d88014fcb7168010010');
INSERT INTO `alone_auth_postaction` VALUES ('402888a84fcb6d88014fcb71ec240011', '40288b854a38408e014a384c4f3c0002', '402888a84fcb6d88014fcb7168010010');
INSERT INTO `alone_auth_postaction` VALUES ('402888a84fcb96ee014fcba1d5190014', '40288b854a38408e014a384c4f3c0002', '402888a84fcb6d88014fcb7293220014');
INSERT INTO `alone_auth_postaction` VALUES ('402888a84fcb96ee014fcba1d5190015', '40288b854a3daac8014a3dfd49a00004', '402888a84fcb6d88014fcb7293220014');
INSERT INTO `alone_auth_postaction` VALUES ('402888a84fcb96ee014fcba1d5190016', '40288b854a3daac8014a3dfe03990006', '402888a84fcb6d88014fcb7293220014');
INSERT INTO `alone_auth_postaction` VALUES ('402888a84fcb96ee014fcbaaf5ba0021', '402894ca4add11f6014add18db1f0009', '402888a84fcb96ee014fcbaa88250020');
INSERT INTO `alone_auth_postaction` VALUES ('402888a84fcb96ee014fcbaaf5ba0022', '402894ca4add11f6014add19da9d000b', '402888a84fcb96ee014fcbaa88250020');

-- ----------------------------
-- Table structure for `alone_auth_user`
-- ----------------------------
DROP TABLE IF EXISTS `alone_auth_user`;
CREATE TABLE `alone_auth_user` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(64) NOT NULL,
  `PASSWORD` varchar(32) NOT NULL COMMENT 'MD5(password+loginname)',
  `COMMENTS` varchar(128) DEFAULT NULL,
  `TYPE` char(1) DEFAULT NULL COMMENT '1:系统用户:2其他3超级用户',
  `CTIME` varchar(14) NOT NULL,
  `UTIME` varchar(14) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `MUSER` varchar(32) NOT NULL,
  `STATE` char(1) NOT NULL,
  `LOGINNAME` varchar(64) NOT NULL,
  `LOGINTIME` varchar(14) DEFAULT NULL,
  `IMGID` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='InnoDB free: 12288 kB alone_user';

-- ----------------------------
-- Records of alone_auth_user
-- ----------------------------
INSERT INTO `alone_auth_user` VALUES ('40288b854a329988014a329a12f30002', '系统管理员', '45A6964B87BEC90B5B6C6414FAF397A7', '', '3', '20141210130925', '20151016104101', 'userId', '40288b854a329988014a329a12f30002', '1', 'sysadmin', '20151016190105', '402888ac507020140150702181ad0017');
INSERT INTO `alone_auth_user` VALUES ('402888304dff84a9014e042a04640010', '789', '594520E10C884B5565C985D53BE81136', null, '3', '20150618085539', '20151016104052', 'NONE', '40288b854a329988014a329a12f30002', '1', 'admin', '20151013191536', '402888ac501d8e0801501d9c65210012');
INSERT INTO `alone_auth_user` VALUES ('402888304dff84a9014e24a16d950029', '赵乃霞', '4A85E0E96A48D488C154E947F3EB41DA', null, '1', '20150624161355', '20150919130820', 'NONE', '40288b854a329988014a329a12f30002', '2', '402888304dff84a9014e24a16d950029', '20150810092735', '');
INSERT INTO `alone_auth_user` VALUES ('402888304e7054a6014e8f41c4660006', '董欢', 'C192C995520F0FF5511EB2E33D35B75E', null, '1', '20150715090848', '20150919130820', 'NONE', '40288b854a329988014a329a12f30002', '2', '402888304e7054a6014e8f41c4660006', '20150727121010', '');
INSERT INTO `alone_auth_user` VALUES ('402888304f5d6240014f6dfbb68c0003', '张虎成', 'C0954032A3BA397F9CAA999FA7A00419', null, '1', '20150827150736', '20150919130820', 'NONE', '40288b854a329988014a329a12f30002', '2', '402888304f5d6240014f6dfbb68c0003', '20150827150737', '');
INSERT INTO `alone_auth_user` VALUES ('402888a84fc075e0014fc0a1867a0108', '11', '1BBD886460827015E5D605ED44252251', '', '1', '20150912161735', '20150914184710', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '2', '402888a84fc075e0014fc0a1867a0108', null, null);
INSERT INTO `alone_auth_user` VALUES ('402888a84fcb75ee014fcb777f050009', '11', '1BBD886460827015E5D605ED44252251', '', '1', '20150914184730', '20150914192335', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '2', '402888a84fcb75ee014fcb777f050009', null, null);
INSERT INTO `alone_auth_user` VALUES ('402888a84fcb75ee014fcb78006c000a', '22', 'C0C473BEE87A482F312AC656B1B6843D', '', '1', '20150914184803', '20150914192335', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '2', '402888a84fcb75ee014fcb78006c000a', null, null);
INSERT INTO `alone_auth_user` VALUES ('402888a84fcb96ee014fcb990498000a', '11', '1BBD886460827015E5D605ED44252251', '', '1', '20150914192401', '20150914193024', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '2', '402888a84fcb96ee014fcb990498000a', null, null);
INSERT INTO `alone_auth_user` VALUES ('402888a84fcb96ee014fcb9d97d8000d', '22', 'C0C473BEE87A482F312AC656B1B6843D', '', '1', '20150914192907', '20150914193024', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '2', '402888a84fcb96ee014fcb9d97d8000d', null, null);
INSERT INTO `alone_auth_user` VALUES ('402888a84fcb96ee014fcb9f05c30010', '11', '1BBD886460827015E5D605ED44252251', '11', '1', '20150914193040', '20150919130820', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '2', '402888a84fcb96ee014fcb9f05c30010', '20150914193526', null);
INSERT INTO `alone_auth_user` VALUES ('402888a84fd07d97014fd07dcdc10009', '张虎成', 'F6263AC8CD766985B680A7998EFB3727', null, '1', '20150915181229', '20150919130820', 'qq1247', '40288b854a329988014a329a12f30002', '2', '402888a84fd07d97014fd07dcdc10009', '20150919124400', '');
INSERT INTO `alone_auth_user` VALUES ('402888a84fd07d97014fd08140b3000c', '张虎成', '675C69D87E49EB67CD02FD66755052DF', null, '1', '20150915181615', '20150919130820', 'qq1247qq', '40288b854a329988014a329a12f30002', '2', '402888a84fd07d97014fd08140b3000c', null, '');
INSERT INTO `alone_auth_user` VALUES ('402888a84fd07d97014fd0821c93000f', '张虎成', 'AED18E023CA324E4F4D61137DE210DA4', null, '1', '20150915181712', '20150919130820', 'qq1247qqqq', '40288b854a329988014a329a12f30002', '2', '402888a84fd07d97014fd0821c93000f', null, '');
INSERT INTO `alone_auth_user` VALUES ('402888a84fd07d97014fd08267780012', '张虎成', '09962BC25D32CCC4C1E1BCA9D8FC83B7', null, '1', '20150915181731', '20150919130820', 'qqqq1247qqqq', '40288b854a329988014a329a12f30002', '2', '402888a84fd07d97014fd08267780012', null, '');
INSERT INTO `alone_auth_user` VALUES ('402888a84fd07d97014fd08389da0015', '张_hc', '33DE0EAB9570E0A0F5235E60DA33A7C4', null, '1', '20150915181845', '20150919130820', 'zhhc', '40288b854a329988014a329a12f30002', '2', '402888a84fd07d97014fd08389da0015', '20150915181855', '');
INSERT INTO `alone_auth_user` VALUES ('402888a84fd0a144014fd0a21257000b', '张虎成2', '3228648A91B3FD49B4F46838C4AD386B', null, '1', '20150915185206', '20150919130820', 'zhanghucheng', '40288b854a329988014a329a12f30002', '2', '402888a84fd0a144014fd0a21257000b', '20150915185512', '402888a84fd0a144014fd0a20deb000a');
INSERT INTO `alone_auth_user` VALUES ('402888ac4fe361ba014fe36d9e0a0009', 'asdfasdf', '6A204BD89F3C8348AFD5C77C717A097A', null, '1', '20150919102736', '20150919130820', 'asdfasdf', '40288b854a329988014a329a12f30002', '2', '402888ac4fe361ba014fe36d9e0a0009', null, '');
INSERT INTO `alone_auth_user` VALUES ('402888ac4fe37f7e014fe3a2b570000d', '123123', '4297F44B13955235245B2497399D7A93', null, '1', '20150919112535', '20150919130820', '123123', '40288b854a329988014a329a12f30002', '2', '402888ac4fe37f7e014fe3a2b570000d', null, '');
INSERT INTO `alone_auth_user` VALUES ('402888a84fe3a5ae014fe3b1b87b000d', 'qqq', '7C7F3EDD5DC256C8D9BEEB63F4D35812', null, '1', '20150919114159', '20150919130820', 'qqqqqq', '40288b854a329988014a329a12f30002', '2', '402888a84fe3a5ae014fe3b1b87b000d', null, '');
INSERT INTO `alone_auth_user` VALUES ('402888a84fe3a5ae014fe3f7fd820028', 'zhc', 'DF5252D2B48DEE7DBCCC03D7C49E273E', null, '1', '20150919125844', '20150919130820', 'qq1248', '40288b854a329988014a329a12f30002', '2', '402888a84fe3a5ae014fe3f7fd820028', null, '402888a84fe3a5ae014fe3f5bd460027');
INSERT INTO `alone_auth_user` VALUES ('402888a84fe3a5ae014fe3f97f34002c', '111', 'BC078B6B4B543EF3CD4CE750F8E14B49', null, '1', '20150919130023', '20150919130820', 'qq1249', '40288b854a329988014a329a12f30002', '2', '402888a84fe3a5ae014fe3f97f34002c', null, '402888a84fe3a5ae014fe3f95130002b');
INSERT INTO `alone_auth_user` VALUES ('402888a84fe3a5ae014fe3fac51c0030', '张虎成qq1246', 'E17A539D08BA89FB6EF0AFA1C93B1B60', null, '1', '20150919130146', '20150919130820', 'qq1246', '40288b854a329988014a329a12f30002', '2', '402888a84fe3a5ae014fe3fac51c0030', '20150919130239', '402888a84fe3a5ae014fe3fac145002f');
INSERT INTO `alone_auth_user` VALUES ('402888a84fe3a5ae014fe4024d3d0038', '张虎成qq1247', 'F6263AC8CD766985B680A7998EFB3727', null, '1', '20150919131000', '20150919131000', 'qq1247', 'qq1247', '1', 'qq1247', '20151015181933', '402888a8501dc09501501e1f3217006f');
INSERT INTO `alone_auth_user` VALUES ('402888a84fe3a5ae014fe4036190003c', '张虎成qq1246', 'E17A539D08BA89FB6EF0AFA1C93B1B60', null, '1', '20150919131110', '20150919131110', 'qq1246', 'qq1246', '1', 'qq1246', '20150927170413', '402888a84fe3a5ae014fe4035ce9003b');

-- ----------------------------
-- Table structure for `alone_auth_userorg`
-- ----------------------------
DROP TABLE IF EXISTS `alone_auth_userorg`;
CREATE TABLE `alone_auth_userorg` (
  `ID` varchar(32) NOT NULL,
  `USERID` varchar(32) NOT NULL,
  `ORGANIZATIONID` varchar(32) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='InnoDB free: 12288 kB; (`organizationid`) REFER `alone/alone';

-- ----------------------------
-- Records of alone_auth_userorg
-- ----------------------------
INSERT INTO `alone_auth_userorg` VALUES ('402888a84fcb96ee014fcb9905ff000b', '402888a84fcb96ee014fcb990498000a', '402888a84fcb6d88014fcb70eb4d000e');
INSERT INTO `alone_auth_userorg` VALUES ('402888a84fcb96ee014fcb9d97d8000e', '402888a84fcb96ee014fcb9d97d8000d', '402888a84fcb6d88014fcb7101da000f');
INSERT INTO `alone_auth_userorg` VALUES ('402888a84fcb96ee014fcba2c60e0019', '402888a84fcb96ee014fcb9f05c30010', '402888a84fcb6d88014fcb70eb4d000e');
INSERT INTO `alone_auth_userorg` VALUES ('402888a84fd07d97014fd07dcdc1000a', '402888a84fd07d97014fd07dcdc10009', '402888a84fcb6d88014fcb6f3e90000a');
INSERT INTO `alone_auth_userorg` VALUES ('402888a84fd07d97014fd08140b3000d', '402888a84fd07d97014fd08140b3000c', '402888a84fcb6d88014fcb6f3e90000a');
INSERT INTO `alone_auth_userorg` VALUES ('402888a84fd07d97014fd0821c930010', '402888a84fd07d97014fd0821c93000f', '402888a84fcb6d88014fcb6f3e90000a');
INSERT INTO `alone_auth_userorg` VALUES ('402888a84fd07d97014fd08267780013', '402888a84fd07d97014fd08267780012', '402888a84fcb6d88014fcb6f3e90000a');
INSERT INTO `alone_auth_userorg` VALUES ('402888a84fd07d97014fd08389da0016', '402888a84fd07d97014fd08389da0015', '402888a84fcb6d88014fcb6f3e90000a');
INSERT INTO `alone_auth_userorg` VALUES ('402888a84fd0a144014fd0a21257000c', '402888a84fd0a144014fd0a21257000b', '402888a84fcb6d88014fcb6f3e90000a');
INSERT INTO `alone_auth_userorg` VALUES ('402888a84fe3a5ae014fe3b1b87b000e', '402888a84fe3a5ae014fe3b1b87b000d', '402888a84fcb6d88014fcb6f3e90000a');
INSERT INTO `alone_auth_userorg` VALUES ('402888a84fe3a5ae014fe3f7fd820029', '402888a84fe3a5ae014fe3f7fd820028', '402888a84fcb6d88014fcb6f3e90000a');
INSERT INTO `alone_auth_userorg` VALUES ('402888a84fe3a5ae014fe3f97f34002d', '402888a84fe3a5ae014fe3f97f34002c', '402888a84fcb6d88014fcb6f3e90000a');
INSERT INTO `alone_auth_userorg` VALUES ('402888a84fe3a5ae014fe3fac51c0031', '402888a84fe3a5ae014fe3fac51c0030', '402888a84fcb6d88014fcb6f3e90000a');
INSERT INTO `alone_auth_userorg` VALUES ('402888a84fe3a5ae014fe4024d3d0039', '402888a84fe3a5ae014fe4024d3d0038', '402888a84fcb6d88014fcb6f3e90000a');
INSERT INTO `alone_auth_userorg` VALUES ('402888a84fe3a5ae014fe4036190003d', '402888a84fe3a5ae014fe4036190003c', '402888a84fcb6d88014fcb6f3e90000a');
INSERT INTO `alone_auth_userorg` VALUES ('402888ac4fe361ba014fe36d9e0a000a', '402888ac4fe361ba014fe36d9e0a0009', '402888a84fcb6d88014fcb6f3e90000a');
INSERT INTO `alone_auth_userorg` VALUES ('402888ac4fe37f7e014fe3a2b570000e', '402888ac4fe37f7e014fe3a2b570000d', '402888a84fcb6d88014fcb6f3e90000a');
INSERT INTO `alone_auth_userorg` VALUES ('402888ac506e7d9401506e857a2d000e', '402888304dff84a9014e042a04640010', '402888a84fcb6d88014fcb6f3e90000a');
INSERT INTO `alone_auth_userorg` VALUES ('402888ac506e7d9401506e859a670010', '40288b854a329988014a329a12f30002', '402888a84fcb6d88014fcb6f3e90000a');

-- ----------------------------
-- Table structure for `alone_auth_userpost`
-- ----------------------------
DROP TABLE IF EXISTS `alone_auth_userpost`;
CREATE TABLE `alone_auth_userpost` (
  `ID` varchar(32) NOT NULL,
  `USERID` varchar(32) NOT NULL,
  `POSTID` varchar(32) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of alone_auth_userpost
-- ----------------------------
INSERT INTO `alone_auth_userpost` VALUES ('402888a84fcb96ee014fcb990a52000c', '402888a84fcb96ee014fcb990498000a', '');
INSERT INTO `alone_auth_userpost` VALUES ('402888a84fcb96ee014fcb9d97d8000f', '402888a84fcb96ee014fcb9d97d8000d', '');
INSERT INTO `alone_auth_userpost` VALUES ('402888a84fcb96ee014fcba2c60e001a', '402888a84fcb96ee014fcb9f05c30010', '402888a84fcb6d88014fcb7293220014');
INSERT INTO `alone_auth_userpost` VALUES ('402888a84fcb96ee014fcba2c60e001b', '402888a84fcb96ee014fcb9f05c30010', '402888a84fcb6d88014fcb7168010010');
INSERT INTO `alone_auth_userpost` VALUES ('402888a84fd07d97014fd07dcdc1000b', '402888a84fd07d97014fd07dcdc10009', '402888a84fcb96ee014fcbaa88250020');
INSERT INTO `alone_auth_userpost` VALUES ('402888a84fd07d97014fd08140b3000e', '402888a84fd07d97014fd08140b3000c', '402888a84fcb96ee014fcbaa88250020');
INSERT INTO `alone_auth_userpost` VALUES ('402888a84fd07d97014fd0821c930011', '402888a84fd07d97014fd0821c93000f', '402888a84fcb96ee014fcbaa88250020');
INSERT INTO `alone_auth_userpost` VALUES ('402888a84fd07d97014fd08267780014', '402888a84fd07d97014fd08267780012', '402888a84fcb96ee014fcbaa88250020');
INSERT INTO `alone_auth_userpost` VALUES ('402888a84fd07d97014fd08389da0017', '402888a84fd07d97014fd08389da0015', '402888a84fcb96ee014fcbaa88250020');
INSERT INTO `alone_auth_userpost` VALUES ('402888a84fd0a144014fd0a21257000d', '402888a84fd0a144014fd0a21257000b', '402888a84fcb96ee014fcbaa88250020');
INSERT INTO `alone_auth_userpost` VALUES ('402888a84fe3a5ae014fe3b1b87b000f', '402888a84fe3a5ae014fe3b1b87b000d', '402888a84fcb96ee014fcbaa88250020');
INSERT INTO `alone_auth_userpost` VALUES ('402888a84fe3a5ae014fe3f7fd82002a', '402888a84fe3a5ae014fe3f7fd820028', '402888a84fcb96ee014fcbaa88250020');
INSERT INTO `alone_auth_userpost` VALUES ('402888a84fe3a5ae014fe3f97f34002e', '402888a84fe3a5ae014fe3f97f34002c', '402888a84fcb96ee014fcbaa88250020');
INSERT INTO `alone_auth_userpost` VALUES ('402888a84fe3a5ae014fe3fac51c0032', '402888a84fe3a5ae014fe3fac51c0030', '402888a84fcb96ee014fcbaa88250020');
INSERT INTO `alone_auth_userpost` VALUES ('402888a84fe3a5ae014fe4024d3d003a', '402888a84fe3a5ae014fe4024d3d0038', '402888a84fcb96ee014fcbaa88250020');
INSERT INTO `alone_auth_userpost` VALUES ('402888a84fe3a5ae014fe4036190003e', '402888a84fe3a5ae014fe4036190003c', '402888a84fcb96ee014fcbaa88250020');
INSERT INTO `alone_auth_userpost` VALUES ('402888ac4fe361ba014fe36d9e0a000b', '402888ac4fe361ba014fe36d9e0a0009', '402888a84fcb96ee014fcbaa88250020');
INSERT INTO `alone_auth_userpost` VALUES ('402888ac4fe37f7e014fe3a2b571000f', '402888ac4fe37f7e014fe3a2b570000d', '402888a84fcb96ee014fcbaa88250020');
INSERT INTO `alone_auth_userpost` VALUES ('402888ac506e7d9401506e857a2e000f', '402888304dff84a9014e042a04640010', '402888a84fcb96ee014fcbaa88250020');
INSERT INTO `alone_auth_userpost` VALUES ('402888ac506e7d9401506e859a680011', '40288b854a329988014a329a12f30002', '402888a84fcb96ee014fcbaa88250020');

-- ----------------------------
-- Table structure for `alone_dictionary_entity`
-- ----------------------------
DROP TABLE IF EXISTS `alone_dictionary_entity`;
CREATE TABLE `alone_dictionary_entity` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(12) NOT NULL,
  `UTIME` varchar(12) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `MUSER` varchar(32) NOT NULL,
  `STATE` char(1) NOT NULL DEFAULT '',
  `NAME` varchar(128) NOT NULL,
  `ENTITYINDEX` varchar(128) NOT NULL,
  `COMMENTS` varchar(128) DEFAULT NULL,
  `TYPE` char(1) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='InnoDB free: 12288 kB';

-- ----------------------------
-- Records of alone_dictionary_entity
-- ----------------------------

-- ----------------------------
-- Table structure for `alone_dictionary_type`
-- ----------------------------
DROP TABLE IF EXISTS `alone_dictionary_type`;
CREATE TABLE `alone_dictionary_type` (
  `CTIME` varchar(12) NOT NULL,
  `UTIME` varchar(12) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `MUSER` varchar(32) NOT NULL,
  `STATE` char(1) NOT NULL,
  `NAME` varchar(128) NOT NULL,
  `COMMENTS` varchar(128) DEFAULT NULL,
  `ENTITYTYPE` varchar(128) NOT NULL,
  `ENTITY` varchar(32) NOT NULL,
  `ID` varchar(32) NOT NULL DEFAULT '',
  `SORT` int(11) NOT NULL,
  `PARENTID` varchar(32) DEFAULT NULL,
  `TREECODE` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_Reference_8` (`ENTITY`),
  CONSTRAINT `FK_Reference_8` FOREIGN KEY (`ENTITY`) REFERENCES `alone_dictionary_entity` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='InnoDB free: 12288 kB alone_dictionary_type';

-- ----------------------------
-- Records of alone_dictionary_type
-- ----------------------------

-- ----------------------------
-- Table structure for `alone_parameter`
-- ----------------------------
DROP TABLE IF EXISTS `alone_parameter`;
CREATE TABLE `alone_parameter` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(12) NOT NULL,
  `UTIME` varchar(12) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `MUSER` varchar(32) NOT NULL,
  `NAME` varchar(128) NOT NULL,
  `STATE` char(1) NOT NULL,
  `PKEY` varchar(64) NOT NULL,
  `PVALUE` varchar(2048) NOT NULL,
  `RULES` varchar(256) DEFAULT NULL,
  `DOMAIN` varchar(64) DEFAULT NULL,
  `COMMENTS` varchar(256) DEFAULT NULL,
  `VTYPE` char(1) NOT NULL COMMENT ' 文本：1 枚举：2',
  `USERABLE` varchar(1) NOT NULL COMMENT '0否，1是',
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='InnoDB free: 12288 kB; InnoDB free: 12288 kB alone_parameter';

-- ----------------------------
-- Records of alone_parameter
-- ----------------------------

-- ----------------------------
-- Table structure for `alone_parameter_local`
-- ----------------------------
DROP TABLE IF EXISTS `alone_parameter_local`;
CREATE TABLE `alone_parameter_local` (
  `ID` varchar(32) NOT NULL,
  `PARAMETERID` varchar(32) NOT NULL,
  `EUSER` varchar(32) NOT NULL,
  `PVALUE` varchar(2048) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_Reference_50` (`PARAMETERID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of alone_parameter_local
-- ----------------------------
INSERT INTO `alone_parameter_local` VALUES ('402894ca4a28a8b7014a28ab90030002', '402894ca4a285625014a28a192c30002', 'userId', 'true');

-- ----------------------------
-- Table structure for `farm_doc`
-- ----------------------------
DROP TABLE IF EXISTS `farm_doc`;
CREATE TABLE `farm_doc` (
  `TITLE` varchar(256) NOT NULL,
  `DOCDESCRIBE` varchar(256) DEFAULT NULL,
  `AUTHOR` varchar(64) DEFAULT NULL,
  `PUBTIME` varchar(14) NOT NULL,
  `DOMTYPE` varchar(2) NOT NULL COMMENT '1.HTML文档，2.txt，3html站点',
  `SHORTTITLE` varchar(64) DEFAULT NULL,
  `TAGKEY` varchar(1024) DEFAULT NULL,
  `SOURCE` varchar(256) DEFAULT NULL,
  `TOPLEVE` int(11) DEFAULT NULL,
  `IMGID` varchar(32) DEFAULT NULL,
  `STATE` varchar(2) NOT NULL COMMENT '1开放、0禁用、2待审核',
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `ETIME` varchar(16) NOT NULL,
  `CUSERNAME` varchar(64) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `EUSERNAME` varchar(64) NOT NULL,
  `EUSER` varchar(32) NOT NULL,
  `PCONTENT` varchar(128) DEFAULT NULL,
  `TEXTID` varchar(32) NOT NULL,
  `WRITEPOP` varchar(2) NOT NULL COMMENT '1公开、0本人、2小组、3禁止编辑',
  `READPOP` varchar(2) NOT NULL COMMENT '1公开、0本人、2小组、3禁用查看',
  `RUNINFOID` varchar(32) NOT NULL,
  `DOCGROUPID` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_Reference_13` (`IMGID`),
  KEY `FK_Reference_16` (`RUNINFOID`),
  KEY `FK_Reference_20` (`DOCGROUPID`),
  CONSTRAINT `FK_Reference_13` FOREIGN KEY (`IMGID`) REFERENCES `farm_docfile` (`ID`),
  CONSTRAINT `FK_Reference_16` FOREIGN KEY (`RUNINFOID`) REFERENCES `farm_docruninfo` (`ID`),
  CONSTRAINT `FK_Reference_20` FOREIGN KEY (`DOCGROUPID`) REFERENCES `farm_docgroup` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of farm_doc
-- ----------------------------
INSERT INTO `farm_doc` VALUES ('WCP3系统简介', '本项目的应用场景是管理业务团队的相关知识（API、代码片段、知识定义、技术经验...）但是其应用并不局限于这些应用。其实这就是一个知识库、知识管理系统、以及若干基于知识的工具和视图。知识发布、知识展示历史版本查看目录章节展示源代码展示文档好评差评操作知识评', '系统管理员', '20151016180957', '1', '', ',知识,好评,权限,管理,用户,小组,文档,x ,发布,使用', '', null, '402888ac5070201401507020eee8000e', '1', '402888ac506eaf5b01506ebfe7aa000b', '20151016114441', '20151016181020', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', null, '402888ac506eaf5b01506ebfe6e1000a', '0', '1', '402888ac506eaf5b01506ebfe6e10009', '402888ac506f5b7401506f9463980029');
INSERT INTO `farm_doc` VALUES ('licence', '&nbsp;&nbsp;&nbsp;&nbsp;WCP当前版本（开源版）允许任何人进行自由下载和自由传播以及使用，但是未经作者同意禁止任何人通过任何手段和名目向使用着索取费用。请谨慎使用由于当前版本的测试规模以及本人的测试能力限制，在本系统（WCP2标准版', '系统管理员', '20151016153141', '1', null, ',使用,系统,过程中,可能,可能会,使用过程中,测试,数据,过程,任何', null, null, null, '1', '402888ac506f5b7401506f8fb8860021', '20151016153141', '20151016190121', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', null, '402888ac506f5b7401506f8fb87a0020', '0', '1', '402888ac506f5b7401506f8fb87a001f', null);
INSERT INTO `farm_doc` VALUES ('古怪知识', '从来不需要想起，永远也不会忘记。注意：本组部分知识仅供参考，使用时请谨慎考证。', '系统管理员', '20151016153647', '4', null, ',参考,从来,注意,不需,仅供,不需要,要想,部分,仅供参考,永远', null, null, null, '1', '402888ac506f5b7401506f94639a002d', '20151016153647', '20151016153647', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', null, '402888ac506f5b7401506f946399002c', '2', '2', '402888ac506f5b7401506f946399002b', '402888ac506f5b7401506f9463980029');
INSERT INTO `farm_doc` VALUES ('知识管理', '知识管理知识管理是企业管理的一项重要内容，主流商业管理课程如EMBA、及MBA等均将“知识管理”作为一项管理者的必备技能要求包含在内。中文名知识管理外文名KnowledgeManagement简&nbsp;&nbsp;&nbsp;&nbsp;称KM原&nb', '系统管理员', '20151016153947', '1', null, '管理,知识,企业,项目,组织,内容,进行,系统,目标,实施', null, null, null, '1', '402888ac506f5b7401506f9724c70040', '20151016153947', '20151016153947', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', null, '402888ac506f5b7401506f9724bd003f', '0', '1', '402888ac506f5b7401506f9724bd003e', '402888ac506f5b7401506f9463980029');

-- ----------------------------
-- Table structure for `farm_docenjoy`
-- ----------------------------
DROP TABLE IF EXISTS `farm_docenjoy`;
CREATE TABLE `farm_docenjoy` (
  `ID` varchar(32) NOT NULL,
  `DOCID` varchar(32) NOT NULL,
  `USERID` varchar(32) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_Reference_17` (`DOCID`),
  CONSTRAINT `FK_Reference_17` FOREIGN KEY (`DOCID`) REFERENCES `farm_doc` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of farm_docenjoy
-- ----------------------------

-- ----------------------------
-- Table structure for `farm_docfile`
-- ----------------------------
DROP TABLE IF EXISTS `farm_docfile`;
CREATE TABLE `farm_docfile` (
  `DIR` varchar(256) NOT NULL,
  `SERVERID` varchar(32) NOT NULL,
  `TYPE` varchar(2) NOT NULL COMMENT '1图片',
  `NAME` varchar(64) NOT NULL,
  `EXNAME` varchar(16) NOT NULL,
  `LEN` float NOT NULL,
  `FILENAME` varchar(64) NOT NULL,
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `ETIME` varchar(16) NOT NULL,
  `CUSERNAME` varchar(64) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `EUSERNAME` varchar(64) NOT NULL,
  `EUSER` varchar(32) NOT NULL,
  `PCONTENT` varchar(128) DEFAULT NULL,
  `PSTATE` varchar(2) NOT NULL COMMENT '1正常、0临时',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of farm_docfile
-- ----------------------------
INSERT INTO `farm_docfile` VALUES ('\\2015\\10\\16\\11\\', '4536704b9982426d81c5dfbac77b90fc', '1', 'wcp3de.png', '.png', '126690', 'upload_bed2d576_6c5c_4737_a2d4_04a493018508_00000000.tmp', '402888ac506eaf5b01506ec1be3d000f', '20151016114642', '20151016114642', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', null, '0');
INSERT INTO `farm_docfile` VALUES ('\\2015\\10\\16\\14\\', '0989ce491ca14d7ca39aed0b6f7d1e77', '1', 'wcp3de.png', '.png', '126690', 'upload_09ef4006_52e9_4dda_9105_9d65be83e80b_00000000.tmp', '402888ac506f5b7401506f5c35360009', '20151016143525', '20151016143525', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', null, '0');
INSERT INTO `farm_docfile` VALUES ('\\2015\\10\\16\\15\\', '195ae639de4343438f674b0f21594ab0', '1', 'QQ截图20150929205819.png', '.png', '26586', 'upload_09ef4006_52e9_4dda_9105_9d65be83e80b_00000001.tmp', '402888ac506f5b7401506f91df320028', '20151016153402', '20151016153647', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', null, '1');
INSERT INTO `farm_docfile` VALUES ('\\2015\\10\\16\\15\\', '6c214bbaa4ec44f1aad7e29d066ffe5f', '0', '938cd292-7a4d-46c4-9e04-cc3ce11fe24e.gif', 'gif', '4537', '938cd292-7a4d-46c4-9e04-cc3ce11fe24e.tmp', '402888ac506f5b7401506f95caf50030', '20151016153819', '20151016153819', 'NONE', 'NONE', 'NONE', 'NONE', 'http://baike.baidu.com/subview/2057/data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAMAAAAoyzS7AAAAGXRFWHRTb2Z0d2FyZQBBZG', '0');
INSERT INTO `farm_docfile` VALUES ('\\2015\\10\\16\\15\\', 'e88839e1ac3e4166ae696febbec49744', '0', '16a06279-ff20-4694-8c8a-de46a8a3776d.gif', 'gif', '4537', '16a06279-ff20-4694-8c8a-de46a8a3776d.tmp', '402888ac506f5b7401506f95cb570031', '20151016153819', '20151016153819', 'NONE', 'NONE', 'NONE', 'NONE', 'http://baike.baidu.com/subview/2057/data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAMAAAAoyzS7AAAAGXRFWHRTb2Z0d2FyZQBBZG', '0');
INSERT INTO `farm_docfile` VALUES ('\\2015\\10\\16\\15\\', '7007de8244874905aceecda39e98119e', '0', '781acc36-561b-48ed-8c35-ba4f23a6e7f4.gif', 'gif', '4537', '781acc36-561b-48ed-8c35-ba4f23a6e7f4.tmp', '402888ac506f5b7401506f95cbc80032', '20151016153819', '20151016153819', 'NONE', 'NONE', 'NONE', 'NONE', 'http://baike.baidu.com/subview/2057/data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAMAAAAoyzS7AAAAGXRFWHRTb2Z0d2FyZQBBZG', '0');
INSERT INTO `farm_docfile` VALUES ('\\2015\\10\\16\\15\\', '4743f5060e8743d38e5357c14c4d519f', '0', '03717aa6-077f-44ba-b68d-7f4dcfd4eebb.gif', 'gif', '4537', '03717aa6-077f-44ba-b68d-7f4dcfd4eebb.tmp', '402888ac506f5b7401506f95cc290033', '20151016153819', '20151016153819', 'NONE', 'NONE', 'NONE', 'NONE', 'http://baike.baidu.com/subview/2057/data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAMAAAAoyzS7AAAAGXRFWHRTb2Z0d2FyZQBBZG', '0');
INSERT INTO `farm_docfile` VALUES ('\\2015\\10\\16\\15\\', '824ab3813f6d48cebfec8c6310338fad', '0', 'd52fbe8d-1f5b-4ac9-b62b-85e648800e6c.jpg', 'jpg', '8726', 'd52fbe8d-1f5b-4ac9-b62b-85e648800e6c.tmp', '402888ac506f5b7401506f95cc9d0034', '20151016153819', '20151016153819', 'NONE', 'NONE', 'NONE', 'NONE', 'http://h.hiphotos.baidu.com/baike/w%3D268/sign=fcda1caa15ce36d3a204843602f23a24/7dd98d1001e9390186506d4e7bec54e736d19674.jpg', '0');
INSERT INTO `farm_docfile` VALUES ('\\2015\\10\\16\\15\\', 'd8efa589c81748f4945f8bc5e0bcf951', '0', 'd75afda3-ad00-467c-b275-bce7d8de7a50.jpg', 'jpg', '4010', 'd75afda3-ad00-467c-b275-bce7d8de7a50.tmp', '402888ac506f5b7401506f95ccf80035', '20151016153819', '20151016153819', 'NONE', 'NONE', 'NONE', 'NONE', 'http://b.hiphotos.baidu.com/baike/whfpf%3D120%2C120%2C50/sign=26d09129c8fcc3ceb4959a73f478e7ba/d4628535e5dde711f12f5efda7efce1b9', '0');
INSERT INTO `farm_docfile` VALUES ('\\2015\\10\\16\\15\\', '7ea4124bb51f41a0be0207434cb2a950', '0', '863a4d49-bcec-40c6-bdbf-acf4caefbf61.png', 'png', '4023', '863a4d49-bcec-40c6-bdbf-acf4caefbf61.tmp', '402888ac506f5b7401506f95cd2a0036', '20151016153819', '20151016153819', 'NONE', 'NONE', 'NONE', 'NONE', 'http://baike.bdimg.com/static/wiki-lemma/widget/lemma_content/configModule/zhixin/img/cityzhixinemptyimg_1a62423.png', '0');
INSERT INTO `farm_docfile` VALUES ('\\2015\\10\\16\\15\\', 'f28d54ee412a43ed81c48e23f87874f7', '0', 'a85e15ec-b69f-4856-afec-065eeeb420b9.jpg', 'jpg', '6452', 'a85e15ec-b69f-4856-afec-065eeeb420b9.tmp', '402888ac506f5b7401506f95cd5b0037', '20151016153819', '20151016153819', 'NONE', 'NONE', 'NONE', 'NONE', 'http://b.hiphotos.baidu.com/baike/whfpf%3D120%2C120%2C50/sign=8b9f8acfe0fe9925cb593a1052956fed/023b5bb5c9ea15ce87657203b6003af33', '0');
INSERT INTO `farm_docfile` VALUES ('\\2015\\10\\16\\15\\', 'b3ad9ae181ba49c882f49be741a2bee7', '0', '530d1d01-268c-4bba-a786-8dcb31651d6f.jpg', 'jpg', '5410', '530d1d01-268c-4bba-a786-8dcb31651d6f.tmp', '402888ac506f5b7401506f95cd950038', '20151016153819', '20151016153819', 'NONE', 'NONE', 'NONE', 'NONE', 'http://e.hiphotos.baidu.com/baike/whfpf%3D120%2C120%2C50/sign=7348d2cbabec8a13144f04a0913ea0bd/0eb30f2442a7d933ecf11257af4bd1137', '0');
INSERT INTO `farm_docfile` VALUES ('\\2015\\10\\16\\15\\', '338753725c3c4644bdcebeeac7e57f3d', '0', 'f0b536ad-1634-4219-9854-ec0c406e337c.jpg', 'jpg', '6134', 'f0b536ad-1634-4219-9854-ec0c406e337c.tmp', '402888ac506f5b7401506f95cdda0039', '20151016153819', '20151016153819', 'NONE', 'NONE', 'NONE', 'NONE', 'http://h.hiphotos.baidu.com/baike/whfpf%3D120%2C120%2C50/sign=26d349e857fbb2fb347e0b5229771196/4afbfbedab64034f07cd83b3adc379310', '0');
INSERT INTO `farm_docfile` VALUES ('\\2015\\10\\16\\15\\', 'bbe9e18d9b854ffd86c1e81fc0ec8c4a', '0', '794aba1d-2d69-4787-8b41-9c0ede5eb138.jpg', 'jpg', '3820', '794aba1d-2d69-4787-8b41-9c0ede5eb138.tmp', '402888ac506f5b7401506f95ce11003a', '20151016153820', '20151016153820', 'NONE', 'NONE', 'NONE', 'NONE', 'http://g.hiphotos.baidu.com/baike/whfpf%3D120%2C120%2C50/sign=7faafdf2ab014c08196e7be56c46333a/38dbb6fd5266d0169faaee73952bd4073', '0');
INSERT INTO `farm_docfile` VALUES ('\\2015\\10\\16\\15\\', '99c6406b170946ca971a9fa07d4564cd', '0', '9ed10c5f-bcc2-4d45-8c68-54db3fc787da.jpg', 'jpg', '5391', '9ed10c5f-bcc2-4d45-8c68-54db3fc787da.tmp', '402888ac506f5b7401506f95ce3b003b', '20151016153820', '20151016153820', 'NONE', 'NONE', 'NONE', 'NONE', 'http://b.hiphotos.baidu.com/baike/whfpf%3D120%2C120%2C0/sign=b27451538f1001e94e69474fde334ade/91529822720e0cf3c67256410846f21fbf', '0');
INSERT INTO `farm_docfile` VALUES ('\\2015\\10\\16\\15\\', 'd185358656474a359dd44ab82067ac6a', '0', '47fe1da2-add6-424f-9be2-3eed63ef2710.jpg', 'jpg', '3964', '47fe1da2-add6-424f-9be2-3eed63ef2710.tmp', '402888ac506f5b7401506f95ce5c003c', '20151016153820', '20151016153820', 'NONE', 'NONE', 'NONE', 'NONE', 'http://g.hiphotos.baidu.com/baike/whfpf%3D120%2C120%2C50/sign=3910d7bc73cf3bc7e8559eacb73d8b93/3801213fb80e7bec0afe28502c2eb9389', '0');
INSERT INTO `farm_docfile` VALUES ('\\2015\\10\\16\\15\\', 'e20dc6a929434ffe8dfbe971e9f6793e', '0', 'ca0df8b4-586b-41c9-a917-08717f532c26.jpg', 'jpg', '8808', 'ca0df8b4-586b-41c9-a917-08717f532c26.tmp', '402888ac506f5b7401506f95ce88003d', '20151016153820', '20151016153820', 'NONE', 'NONE', 'NONE', 'NONE', 'http://f.hiphotos.baidu.com/baike/whfpf%3D120%2C120%2C0/sign=7888e757b7fb43161a4a293a46997711/78310a55b319ebc4f2a0f1058726cffc1f', '0');
INSERT INTO `farm_docfile` VALUES ('\\2015\\10\\16\\15\\', '60a6bb77906c43a89c1fd673a3911fe2', '1', 'QQ截图20150930173627.png', '.png', '29084', 'upload_09ef4006_52e9_4dda_9105_9d65be83e80b_00000002.tmp', '402888ac506f5b7401506f98d57e0048', '20151016154138', '20151016154138', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', null, '0');
INSERT INTO `farm_docfile` VALUES ('\\2015\\10\\16\\18\\', '3a5d95f88c994e76bbc716cb736157bc', '1', 'wcp3de.png', '.png', '126690', 'upload_2ebdc47f_ed8d_4a98_a36a_2d8cf76e86fc_00000000.tmp', '402888ac5070201401507020eee8000e', '20151016181017', '20151016181017', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', null, '0');
INSERT INTO `farm_docfile` VALUES ('\\2015\\10\\16\\18\\', '4bc5ae401d3149fcbeabf895e111a16f', '1', 'QQ截图20150930173627.png', '.png', '29084', 'upload_2ebdc47f_ed8d_4a98_a36a_2d8cf76e86fc_00000001.tmp', '402888ac507020140150702181ad0017', '20151016181055', '20151016181055', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', null, '0');

-- ----------------------------
-- Table structure for `farm_docgroup`
-- ----------------------------
DROP TABLE IF EXISTS `farm_docgroup`;
CREATE TABLE `farm_docgroup` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `ETIME` varchar(16) NOT NULL,
  `CUSERNAME` varchar(64) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `EUSERNAME` varchar(64) NOT NULL,
  `EUSER` varchar(32) NOT NULL,
  `PSTATE` varchar(2) NOT NULL,
  `PCONTENT` varchar(128) DEFAULT NULL,
  `GROUPNAME` varchar(128) NOT NULL,
  `GROUPNOTE` varchar(256) NOT NULL,
  `GROUPTAG` varchar(256) NOT NULL,
  `GROUPIMG` varchar(32) NOT NULL,
  `JOINCHECK` varchar(2) NOT NULL COMMENT '1是0否',
  `USERNUM` int(11) NOT NULL,
  `HOMEDOCID` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of farm_docgroup
-- ----------------------------
INSERT INTO `farm_docgroup` VALUES ('402888ac506f5b7401506f9463980029', '20151016153647', '20151016153647', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '1', null, '古怪知识', '从来不需要想起，永远也不会忘记。\r\n\r\n注意：本组部分知识仅供参考，使用时请谨慎考证。 ', '', '402888ac506f5b7401506f91df320028', '0', '1', '402888ac506f5b7401506f94639a002d');

-- ----------------------------
-- Table structure for `farm_docgroup_user`
-- ----------------------------
DROP TABLE IF EXISTS `farm_docgroup_user`;
CREATE TABLE `farm_docgroup_user` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `ETIME` varchar(16) NOT NULL,
  `CUSERNAME` varchar(64) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `EUSERNAME` varchar(64) NOT NULL,
  `EUSER` varchar(32) NOT NULL,
  `PSTATE` varchar(2) NOT NULL COMMENT '1在用，0邀请，2删除，3申请',
  `PCONTENT` varchar(128) DEFAULT NULL,
  `GROUPID` varchar(32) NOT NULL,
  `USERID` varchar(32) NOT NULL,
  `LEADIS` varchar(2) NOT NULL COMMENT '1是0否',
  `EDITIS` varchar(2) NOT NULL COMMENT '1是0否',
  `SHOWHOME` varchar(2) NOT NULL COMMENT '1是0否',
  `SHOWSORT` int(11) NOT NULL,
  `APPLYNOTE` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_Reference_21` (`GROUPID`),
  CONSTRAINT `FK_Reference_21` FOREIGN KEY (`GROUPID`) REFERENCES `farm_docgroup` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of farm_docgroup_user
-- ----------------------------
INSERT INTO `farm_docgroup_user` VALUES ('402888ac506f5b7401506f946398002a', '20151016153647', '20151016153647', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '1', null, '402888ac506f5b7401506f9463980029', '40288b854a329988014a329a12f30002', '1', '1', '1', '10', null);

-- ----------------------------
-- Table structure for `farm_docmessage`
-- ----------------------------
DROP TABLE IF EXISTS `farm_docmessage`;
CREATE TABLE `farm_docmessage` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `CUSERNAME` varchar(64) NOT NULL,
  `PSTATE` varchar(2) NOT NULL,
  `PCONTENT` varchar(128) DEFAULT NULL,
  `READUSERID` varchar(32) NOT NULL,
  `CONTENT` varchar(256) NOT NULL,
  `TITLE` varchar(64) NOT NULL,
  `APPID` varchar(32) DEFAULT NULL COMMENT '业务公共会依据该ID查询自己相关的留言',
  `READSTATE` varchar(2) NOT NULL COMMENT '0未读、1已读',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of farm_docmessage
-- ----------------------------

-- ----------------------------
-- Table structure for `farm_docruninfo`
-- ----------------------------
DROP TABLE IF EXISTS `farm_docruninfo`;
CREATE TABLE `farm_docruninfo` (
  `ID` varchar(32) NOT NULL,
  `VISITNUM` int(11) NOT NULL,
  `HOTNUM` int(11) NOT NULL,
  `LASTVTIME` varchar(14) NOT NULL,
  `PRAISEYES` int(11) NOT NULL,
  `PRAISENO` int(11) NOT NULL,
  `EVALUATE` int(11) NOT NULL COMMENT '好评减去差评',
  `ANSWERINGNUM` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of farm_docruninfo
-- ----------------------------
INSERT INTO `farm_docruninfo` VALUES ('402888ac506eaf5b01506ebfe6e10009', '8', '98', '201510162006', '0', '0', '0', '0');
INSERT INTO `farm_docruninfo` VALUES ('402888ac506f5b7401506f8fb87a001f', '4', '65', '201510161901', '0', '0', '0', '0');
INSERT INTO `farm_docruninfo` VALUES ('402888ac506f5b7401506f946399002b', '0', '0', '20151016153647', '0', '0', '0', '0');
INSERT INTO `farm_docruninfo` VALUES ('402888ac506f5b7401506f9724bd003e', '1', '31', '201510161540', '0', '0', '0', '0');

-- ----------------------------
-- Table structure for `farm_docruninfo_detail`
-- ----------------------------
DROP TABLE IF EXISTS `farm_docruninfo_detail`;
CREATE TABLE `farm_docruninfo_detail` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `CUSERNAME` varchar(64) DEFAULT NULL,
  `CUSER` varchar(32) DEFAULT NULL,
  `PSTATE` varchar(2) NOT NULL,
  `PCONTENT` varchar(128) DEFAULT NULL,
  `VTYPE` varchar(2) NOT NULL COMMENT '1访问、2好评、3差评',
  `DOCTEXTID` varchar(32) NOT NULL,
  `RUNINFOID` varchar(32) NOT NULL,
  `USERIP` varchar(128) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_Reference_22` (`DOCTEXTID`),
  KEY `FK_Reference_23` (`RUNINFOID`),
  CONSTRAINT `FK_Reference_22` FOREIGN KEY (`DOCTEXTID`) REFERENCES `farm_doctext` (`ID`),
  CONSTRAINT `FK_Reference_23` FOREIGN KEY (`RUNINFOID`) REFERENCES `farm_docruninfo` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of farm_docruninfo_detail
-- ----------------------------

-- ----------------------------
-- Table structure for `farm_doctext`
-- ----------------------------
DROP TABLE IF EXISTS `farm_doctext`;
CREATE TABLE `farm_doctext` (
  `TEXT1` longtext NOT NULL,
  `TEXT2` longtext,
  `TEXT3` longtext,
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `ETIME` varchar(16) NOT NULL,
  `CUSERNAME` varchar(64) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `EUSERNAME` varchar(64) NOT NULL,
  `EUSER` varchar(32) NOT NULL,
  `PCONTENT` varchar(128) DEFAULT NULL,
  `PSTATE` varchar(2) NOT NULL COMMENT '1在用内容。2.版本存档3.待审核',
  `DOCID` varchar(32) DEFAULT NULL COMMENT '存档版本时记录其所对照的文档id',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of farm_doctext
-- ----------------------------
INSERT INTO `farm_doctext` VALUES ('<p>\r\n	本项目的应用场景是管理业务团队的相关知识（API、代码片段、知识定义、技术经验...） 但是其应用并不局限于这些应用。其实这就是一个 知识库、知识管理系统、以及若干基于知识的工具和视图。\r\n</p>\r\n<p>\r\n	<br />\r\n</p>\r\n<h1>\r\n	知识发布、知识展示<br />\r\n</h1>\r\n<ul>\r\n	<li>\r\n		<p>\r\n			历史版本查看\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<p>\r\n			目录章节展示\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<p>\r\n			源代码展示\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<p>\r\n			文档好评差评操作\r\n		</p>\r\n	</li>\r\n	<li>\r\n		知识评论<br />\r\n	</li>\r\n	<li>\r\n		<p>\r\n			全文检索\r\n		</p>\r\n	</li>\r\n	<li>\r\n		上传附件资源\r\n	</li>\r\n	<li>\r\n		热度计算\r\n	</li>\r\n	<li>\r\n		权限控制\r\n	</li>\r\n	<li>\r\n		关联知识推送<br />\r\n	</li>\r\n</ul>\r\n<p>\r\n	<br />\r\n</p>\r\n<h1>\r\n	知识小组<br />\r\n</h1>\r\n<ul>\r\n	<li>\r\n		<p>\r\n			支持整站下载\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<p>\r\n			单个网页下载\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<p>\r\n			对下载内容通过配置的方式抽取有用数据\r\n		</p>\r\n	</li>\r\n</ul>\r\n<h1>\r\n	权限划分<br />\r\n</h1>\r\n<ul>\r\n	<li>\r\n		<p>\r\n			文档访问权限和编辑权限可以分别控制：\r\n		</p>\r\n		<p>\r\n			[高级权限|普通权限]  X  [小组|创建人|公开]  X  [系统中N个小组]  X  [阅读|编辑]\r\n		</p>\r\n	</li>\r\n</ul>\r\n<h1>\r\n	统计分析<br />\r\n</h1>\r\n<ul>\r\n	<li>\r\n		<p>\r\n			用户好评度\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<p>\r\n			小组好评度\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<p>\r\n			用户发布量\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<p>\r\n			文章好评\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<p>\r\n			文章差评\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<p>\r\n			系统日使用量\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<p>\r\n			月使用量\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<p>\r\n			年使用量\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<p>\r\n			文档好评率\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<p>\r\n			文档访问量\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<p>\r\n			用户累计好评\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<p>\r\n			用户累计差评\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<p>\r\n			用户等级等统计项\r\n		</p>\r\n	</li>\r\n</ul>\r\n<h1>\r\n	<strong>WCP3版本说明</strong>\r\n</h1>\r\n<ul>\r\n	<li>\r\n		使用springMVC替换struts2\r\n	</li>\r\n	<li>\r\n		开源farm_Lucene插件\r\n	</li>\r\n	<li>\r\n		代码50%以上重构\r\n	</li>\r\n	<li>\r\n		去除web站点发布功能\r\n	</li>\r\n	<li>\r\n		去除爬虫功能\r\n	</li>\r\n	<li>\r\n		个别页面UI有重新设计\r\n	</li>\r\n	<li>\r\n		去掉计划任务功能<br />\r\n	</li>\r\n</ul>', null, null, '402888ac506eaf5b01506ebfe6e1000a', '20151016181020', '20151016181020', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员修改', '1', null);
INSERT INTO `farm_doctext` VALUES ('<p>\r\n	本项目的应用场景是管理业务团队的相关知识（API、代码片段、知识定义、技术经验...） 但是其应用并不局限于这些应用。其实这就是一个 知识库、知识管理系统、以及若干基于知识的工具和视图。\r\n</p>\r\n<p>\r\n	<h1>\r\n		知识发布、知识展示<br />\r\n	</h1>\r\n	<ul>\r\n		<li>\r\n			<p>\r\n				支持版本管理\r\n			</p>\r\n		</li>\r\n		<li>\r\n			<p>\r\n				目录章节管理\r\n			</p>\r\n		</li>\r\n		<li>\r\n			<p>\r\n				附件管理\r\n			</p>\r\n		</li>\r\n		<li>\r\n			<p>\r\n				源代码发布\r\n			</p>\r\n		</li>\r\n		<li>\r\n			<p>\r\n				文档好评度管理\r\n			</p>\r\n		</li>\r\n		<li>\r\n			<p>\r\n				全文检索\r\n			</p>\r\n		</li>\r\n	</ul>\r\n	<p>\r\n		<br />\r\n	</p>\r\n	<h1>\r\n		知识小组<br />\r\n	</h1>\r\n	<ul>\r\n		<li>\r\n			<p>\r\n				支持整站下载\r\n			</p>\r\n		</li>\r\n		<li>\r\n			<p>\r\n				单个网页下载\r\n			</p>\r\n		</li>\r\n		<li>\r\n			<p>\r\n				对下载内容通过配置的方式抽取有用数据\r\n			</p>\r\n		</li>\r\n	</ul>\r\n	<h1>\r\n		权限划分<br />\r\n	</h1>\r\n	<ul>\r\n		<li>\r\n			<p>\r\n				文档访问权限和编辑权限可以分别控制：\r\n			</p>\r\n			<p>\r\n				[高级权限|普通权限] &nbsp;X &nbsp;[小组|创建人|公开] &nbsp;X &nbsp;[系统中N个小组] &nbsp;X &nbsp;[阅读|编辑]= 12N种权限组合\r\n			</p>\r\n		</li>\r\n	</ul>\r\n	<h1>\r\n		统计分析<br />\r\n	</h1>\r\n	<ul>\r\n		<li>\r\n			<p>\r\n				用户好评度\r\n			</p>\r\n		</li>\r\n		<li>\r\n			<p>\r\n				小组好评度\r\n			</p>\r\n		</li>\r\n		<li>\r\n			<p>\r\n				用户发布量\r\n			</p>\r\n		</li>\r\n		<li>\r\n			<p>\r\n				文章好评\r\n			</p>\r\n		</li>\r\n		<li>\r\n			<p>\r\n				文章差评\r\n			</p>\r\n		</li>\r\n		<li>\r\n			<p>\r\n				系统日使用量\r\n			</p>\r\n		</li>\r\n		<li>\r\n			<p>\r\n				月使用量\r\n			</p>\r\n		</li>\r\n		<li>\r\n			<p>\r\n				年使用量\r\n			</p>\r\n		</li>\r\n		<li>\r\n			<p>\r\n				文档好评率\r\n			</p>\r\n		</li>\r\n		<li>\r\n			<p>\r\n				文档访问量\r\n			</p>\r\n		</li>\r\n		<li>\r\n			<p>\r\n				用户累计好评\r\n			</p>\r\n		</li>\r\n		<li>\r\n			<p>\r\n				用户累计差评\r\n			</p>\r\n		</li>\r\n		<li>\r\n			<p>\r\n				用户等级等统计项\r\n			</p>\r\n		</li>\r\n	</ul>\r\n</p>', null, null, '402888ac506f5b7401506f5c3d3d000a', '20151016114441', '20151016114441', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', 'CREAT', '2', '402888ac506eaf5b01506ebfe7aa000b');
INSERT INTO `farm_doctext` VALUES ('<p>\r\n	本项目的应用场景是管理业务团队的相关知识（API、代码片段、知识定义、技术经验...） 但是其应用并不局限于这些应用。其实这就是一个 知识库、知识管理系统、以及若干基于知识的工具和视图。\r\n</p>\r\n<p>\r\n	<h1>\r\n		知识发布、知识展示<br />\r\n	</h1>\r\n	<ul>\r\n		<li>\r\n			<p>\r\n				支持版本管理\r\n			</p>\r\n		</li>\r\n		<li>\r\n			<p>\r\n				目录章节管理\r\n			</p>\r\n		</li>\r\n		<li>\r\n			<p>\r\n				附件管理\r\n			</p>\r\n		</li>\r\n		<li>\r\n			<p>\r\n				源代码发布\r\n			</p>\r\n		</li>\r\n		<li>\r\n			<p>\r\n				文档好评度管理\r\n			</p>\r\n		</li>\r\n		<li>\r\n			<p>\r\n				全文检索\r\n			</p>\r\n		</li>\r\n	</ul>\r\n	<p>\r\n		<br />\r\n	</p>\r\n	<h1>\r\n		知识小组<br />\r\n	</h1>\r\n	<ul>\r\n		<li>\r\n			<p>\r\n				支持整站下载\r\n			</p>\r\n		</li>\r\n		<li>\r\n			<p>\r\n				单个网页下载\r\n			</p>\r\n		</li>\r\n		<li>\r\n			<p>\r\n				对下载内容通过配置的方式抽取有用数据\r\n			</p>\r\n		</li>\r\n	</ul>\r\n	<h1>\r\n		权限划分<br />\r\n	</h1>\r\n	<ul>\r\n		<li>\r\n			<p>\r\n				文档访问权限和编辑权限可以分别控制：\r\n			</p>\r\n			<p>\r\n				[高级权限|普通权限]  X  [小组|创建人|公开]  X  [系统中N个小组]  X  [阅读|编辑]= 12N种权限组合\r\n			</p>\r\n		</li>\r\n	</ul>\r\n	<h1>\r\n		统计分析<br />\r\n	</h1>\r\n	<ul>\r\n		<li>\r\n			<p>\r\n				用户好评度\r\n			</p>\r\n		</li>\r\n		<li>\r\n			<p>\r\n				小组好评度\r\n			</p>\r\n		</li>\r\n		<li>\r\n			<p>\r\n				用户发布量\r\n			</p>\r\n		</li>\r\n		<li>\r\n			<p>\r\n				文章好评\r\n			</p>\r\n		</li>\r\n		<li>\r\n			<p>\r\n				文章差评\r\n			</p>\r\n		</li>\r\n		<li>\r\n			<p>\r\n				系统日使用量\r\n			</p>\r\n		</li>\r\n		<li>\r\n			<p>\r\n				月使用量\r\n			</p>\r\n		</li>\r\n		<li>\r\n			<p>\r\n				年使用量\r\n			</p>\r\n		</li>\r\n		<li>\r\n			<p>\r\n				文档好评率\r\n			</p>\r\n		</li>\r\n		<li>\r\n			<p>\r\n				文档访问量\r\n			</p>\r\n		</li>\r\n		<li>\r\n			<p>\r\n				用户累计好评\r\n			</p>\r\n		</li>\r\n		<li>\r\n			<p>\r\n				用户累计差评\r\n			</p>\r\n		</li>\r\n		<li>\r\n			<p>\r\n				用户等级等统计项\r\n			</p>\r\n		</li>\r\n	</ul>\r\n</p>', null, null, '402888ac506f5b7401506f5f9d160013', '20151016143527', '20151016143527', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员修改', '2', '402888ac506eaf5b01506ebfe7aa000b');
INSERT INTO `farm_doctext` VALUES ('<p>\r\n	本项目的应用场景是管理业务团队的相关知识（API、代码片段、知识定义、技术经验...） 但是其应用并不局限于这些应用。其实这就是一个 知识库、知识管理系统、以及若干基于知识的工具和视图。\r\n</p>\r\n<p>\r\n	<br />\r\n</p>\r\n<h1>\r\n	知识发布、知识展示<br />\r\n</h1>\r\n<ul>\r\n	<li>\r\n		<p>\r\n			历史版本查看\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<p>\r\n			目录章节展示\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<p>\r\n			源代码展示\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<p>\r\n			文档好评差评操作\r\n		</p>\r\n	</li>\r\n	<li>\r\n		知识评论<br />\r\n	</li>\r\n	<li>\r\n		<p>\r\n			全文检索\r\n		</p>\r\n	</li>\r\n	<li>\r\n		上传附件资源\r\n	</li>\r\n	<li>\r\n		热度计算\r\n	</li>\r\n	<li>\r\n		权限控制\r\n	</li>\r\n	<li>\r\n		关联知识推送<br />\r\n	</li>\r\n</ul>\r\n<p>\r\n	<br />\r\n</p>\r\n<h1>\r\n	知识小组<br />\r\n</h1>\r\n<ul>\r\n	<li>\r\n		<p>\r\n			支持整站下载\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<p>\r\n			单个网页下载\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<p>\r\n			对下载内容通过配置的方式抽取有用数据\r\n		</p>\r\n	</li>\r\n</ul>\r\n<h1>\r\n	权限划分<br />\r\n</h1>\r\n<ul>\r\n	<li>\r\n		<p>\r\n			文档访问权限和编辑权限可以分别控制：\r\n		</p>\r\n		<p>\r\n			[高级权限|普通权限] &nbsp;X &nbsp;[小组|创建人|公开] &nbsp;X &nbsp;[系统中N个小组] &nbsp;X &nbsp;[阅读|编辑]= 12N种权限组合\r\n		</p>\r\n	</li>\r\n</ul>\r\n<h1>\r\n	统计分析<br />\r\n</h1>\r\n<ul>\r\n	<li>\r\n		<p>\r\n			用户好评度\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<p>\r\n			小组好评度\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<p>\r\n			用户发布量\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<p>\r\n			文章好评\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<p>\r\n			文章差评\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<p>\r\n			系统日使用量\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<p>\r\n			月使用量\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<p>\r\n			年使用量\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<p>\r\n			文档好评率\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<p>\r\n			文档访问量\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<p>\r\n			用户累计好评\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<p>\r\n			用户累计差评\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<p>\r\n			用户等级等统计项\r\n		</p>\r\n	</li>\r\n</ul>\r\n<p>\r\n	<br />\r\n</p>', null, null, '402888ac506f5b7401506f8931630019', '20151016143908', '20151016143908', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '修改内容', '2', '402888ac506eaf5b01506ebfe7aa000b');
INSERT INTO `farm_doctext` VALUES ('&nbsp;&nbsp;&nbsp;&nbsp;WCP当前版本（开源版）允许任何人进行自由下载和自由传播以及使用，但是未经作者同意禁止任何人通过任何手段和名目向使用着索取费用。<br />\r\n<h1>\r\n	请谨慎使用\r\n</h1>\r\n<p>\r\n	由于当前版本的测试规模以及本人的测试能力限制，在本系统（WCP2标准版）使用过程中可能会发生未知的运行错误，这些错误可能会导致本软件中的数据问题，最坏情况下可能会丢失已经维护进入系统的数据。\r\n</p>\r\n<h1>\r\n	提示\r\n</h1>\r\n通过有限的测试，可以预料在本软件的安装和使用过程中，不会损坏到操作系统中其它部分的数据和功能（特殊情况未经验证，所以还是会有一定风险）。<br />\r\n如果您在使用过程中遇到难以解决的问题或者是使用中的疑惑，可以通过系统中的联系方式获得帮助。<br />', null, null, '402888ac506f5b7401506f8fb87a0020', '20151016190121', '20151016190121', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '添加索引', '1', null);
INSERT INTO `farm_doctext` VALUES ('从来不需要想起，永远也不会忘记。\r\n\r\n注意：本组部分知识仅供参考，使用时请谨慎考证。 ', null, null, '402888ac506f5b7401506f946399002c', '201510161536', '201510161536', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', 'CREAT', '1', null);
INSERT INTO `farm_doctext` VALUES ('<p style=\"text-indent:2em;\">\r\n	<br />\r\n</p>\r\n<div class=\"body-wrapper\">\r\n	<div class=\"content-wrapper\">\r\n		<div class=\"content\">\r\n			<div class=\"bdsharebuttonbox side-share\">\r\n				<h1>\r\n					知识管理\r\n				</h1>\r\n<a class=\"edit-lemma cmn-btn-hover-blue cmn-btn-28 j-edit-link\"><em class=\"cmn-icon wiki-lemma-icons wiki-lemma-icons_edit-lemma\"></em></a> \r\n				<div class=\"lemma-summary\">\r\n					<div class=\"para\">\r\n						知识管理是企业管理的一项重要内容，主流商业管理课程如 <a target=\"_blank\">EMBA</a>、及 <a target=\"_blank\">MBA</a>等均将“知识管理”作为一项管理者的必备技能要求包含在内。\r\n					</div>\r\n				</div>\r\n				<div class=\"basic-info\">\r\n					中文名知识管理外文名Knowledge Management简&nbsp;&nbsp;&nbsp;&nbsp;称KM原&nbsp;&nbsp;&nbsp;&nbsp;则积累、共享、交流\r\n				</div>\r\n<br />\r\n				<h2 class=\"para-title level-2\">\r\n					<span class=\"title-text\">定义</span> <a class=\"edit-icon j-edit-link\"><em class=\"cmn-icon wiki-lemma-icons wiki-lemma-icons_edit-lemma\"></em></a> \r\n				</h2>\r\n				<div class=\"para\">\r\n					所谓知识管理的定义为，在组织中建构一个量化与质化的知识系统，让 <a target=\"_blank\"><b>组织</b></a>中的资讯与知识，透过获得、创造、分享、整合、记录、存取、更新、创新等过程，不断的回馈到知识系统内，形成永不间断的累积个人与组织的知识成为组织智慧的循环，在企业组织中成为管理与应用的 <a target=\"_blank\">智慧资本</a>，有助于企业做出正确的决策，以适应市场的变迁。 <sup>[1]</sup>&nbsp;\r\n				</div>\r\n				<div class=\"para\">\r\n					21世纪企业的成功越来越依赖于企业所拥有知识的质量，利用企业所拥有的知识为企业创造竞争优势和持续竞争优势对企业来说始终是一个挑战。\r\n				</div>\r\n				<div class=\"anchor-list\">\r\n					<br />\r\n				</div>\r\n				<h3 class=\"para-title level-3\">\r\n					<span class=\"title-text\">知识</span> \r\n				</h3>\r\n				<div class=\"para\">\r\n					通过学习、实践或探索所获得的认识、判断或技能。\r\n				</div>\r\n				<div class=\"para\">\r\n					注1：知识可以是显性的，也可以是隐性的；可以是组织的，也可以是个人的。\r\n				</div>\r\n				<div class=\"para\">\r\n					注2：知识可包括事实知识、原理知识、 <a target=\"_blank\">技能知识</a>和人际知识。\r\n				</div>\r\n				<div class=\"anchor-list\">\r\n					<br />\r\n				</div>\r\n				<h3 class=\"para-title level-3\">\r\n					<span class=\"title-text\">知识管理</span> \r\n				</h3>\r\n				<div class=\"para\">\r\n					对知识、知识创造过程和知识的应用进行规划和管理的活动。\r\n				</div>\r\n				<h3 class=\"para-title level-3\">\r\n					<span class=\"title-text\">组织</span> \r\n				</h3>\r\n				<div class=\"para\">\r\n					职责、权限和相互关系得到安排的一组人员及设施。\r\n				</div>\r\n				<div class=\"para\">\r\n					示例：公司、集团、商行、企事业单位、研究机构、慈善机构、代理商、社团或上述组织的部分或组合。\r\n				</div>\r\n				<div class=\"para\">\r\n					注1：安排通常是有序的。\r\n				</div>\r\n				<div class=\"para\">\r\n					注2：组织可以是公有的或私有的。\r\n				</div>\r\n				<div class=\"anchor-list\">\r\n					<br />\r\n				</div>\r\n				<h3 class=\"para-title level-3\">\r\n					<span class=\"title-text\">管理体系</span> \r\n				</h3>\r\n				<div class=\"para\">\r\n					建立方针和目标并实现这些目标的体系。\r\n				</div>\r\n				<div class=\"anchor-list\">\r\n					<br />\r\n				</div>\r\n				<h3 class=\"para-title level-3\">\r\n					<span class=\"title-text\">显性知识</span> \r\n				</h3>\r\n				<div class=\"para\">\r\n					以文字、符号、图形等方式表达的知识。\r\n				</div>\r\n				<div class=\"anchor-list\">\r\n					<br />\r\n				</div>\r\n				<h3 class=\"para-title level-3\">\r\n					<span class=\"title-text\">隐性知识</span> \r\n				</h3>\r\n				<div class=\"para\">\r\n					未以文字、符号、图形等方式表达的知识，存在于人的大脑中。\r\n				</div>\r\n				<div class=\"anchor-list\">\r\n					<br />\r\n				</div>\r\n				<h2 class=\"para-title level-2\">\r\n					<span class=\"title-text\">特征</span> <a class=\"edit-icon j-edit-link\"><em class=\"cmn-icon wiki-lemma-icons wiki-lemma-icons_edit-lemma\"></em>编辑</a> \r\n				</h2>\r\n				<div class=\"para\">\r\n					<b>在管理理念上</b> \r\n				</div>\r\n				<div class=\"para\">\r\n					知识管理真正体现了以人为本的 <a target=\"_blank\">管理思想</a>，人力资源管理成为组织管理的核心。\r\n				</div>\r\n				<div class=\"para\">\r\n					<b>在管理对象上</b> \r\n				</div>\r\n				<div class=\"para\">\r\n					知识管理以无形资产管理为主要对象，比以往任何管理形式都更加强调 <a target=\"_blank\">知识资产</a>的重要性。\r\n				</div>\r\n				<div class=\"para\">\r\n					<b>在管理内容上</b> \r\n				</div>\r\n				<div class=\"para\">\r\n					要遵循“知识积累——创造——应用——形成知识平台——再积累——再创造——再应用——形成新的知识平台”的循环过程。\r\n				</div>\r\n				<div class=\"para\">\r\n					<b>在范围及重点上</b> \r\n				</div>\r\n				<div class=\"para\">\r\n					知识管理包括 <a target=\"_blank\">显性知识</a>管理和 <a target=\"_blank\">隐性知识</a>管理，但以隐性知识管理为重点，并注重显性知识与隐性知识之间的共享与转换。\r\n				</div>\r\n				<div class=\"para\">\r\n					<b>目标和策略上</b> \r\n				</div>\r\n				<div class=\"para\">\r\n					以知识管理创新为直接目标，已建立知识创新平台为基本策略，智力性和创新性是知识管理的标志性特点。\r\n				</div>\r\n				<div class=\"para\">\r\n					<b>在组织结构上</b> \r\n				</div>\r\n				<div class=\"para\">\r\n					与以往其他管理形式所采取的金字塔式的等级模式不同，知识管理采取开放的、扁平式管理的学习型组织模式。 <sup>[2]</sup> <a name=\"undefined\"></a> \r\n				</div>\r\n				<div class=\"anchor-list\">\r\n					<a name=\"undefined\"></a> <a name=\"undefined\"></a> <a name=\"undefined\"></a> \r\n				</div>\r\n				<h2 class=\"para-title level-2\">\r\n					<span class=\"title-text\">原则组织</span> <a class=\"edit-icon j-edit-link\"><em class=\"cmn-icon wiki-lemma-icons wiki-lemma-icons_edit-lemma\"></em>编辑</a> \r\n				</h2>\r\n				<div class=\"para\">\r\n					知识管理要遵循以下三条原则：(1)积累原则。知识积累是实施知识的管理基础。(2)共享原则。知识共享，是指一个组织内部的信息和知识要尽可能公开，使每一个员工都能接触和使用公司的知识和信息。(3)交流原则。知识管理的核心就是要在公司内部建立一个有利于交流的组织结构和文化气氛，使员工之间的交流毫无障碍。\r\n				</div>\r\n				<div class=\"para\">\r\n					知识积累是实施知识的管理基础；知识共享适时组织的每个成员都能接触和使用公司的知识和信息；知识交流则是使知识体现其价值的关键环节，它在知识管理的三个原则中处于最高层次。\r\n				</div>\r\n				<div class=\"para\">\r\n					按照上述原则进行知识管理，首先就要明确知识管理涉及组织的所有层面和所有部门，一个组织要进行有效的知识管理，关键在于建立起系统的知识管理组织体系。这一体系所实现的功能主要包括以下几个方面：组织能够清楚地了解它已有什么样的知识和需要什么样的知识；组织知识一定要能够及时传递给那些日常工作中只适合需要它们的人；组织知识一定要使那些需要他们的人能够获取；不断生产新知识，并要使整个组织的人能够获取它们；对可靠的、有生命力的知识的引入进行控制；对组织知识进行定期的检测和合法化；通过企业文化的建立和激励措施使知识管理更容易进行。 <sup>[3]</sup> <a name=\"undefined\"></a> \r\n				</div>\r\n				<div class=\"anchor-list\">\r\n					<a name=\"undefined\"></a> <a name=\"undefined\"></a> <a name=\"undefined\"></a> \r\n				</div>\r\n				<h2 class=\"para-title level-2\">\r\n					<span class=\"title-text\">原因</span> <a class=\"edit-icon j-edit-link\"><em class=\"cmn-icon wiki-lemma-icons wiki-lemma-icons_edit-lemma\"></em>编辑</a> \r\n				</h2>\r\n				<div class=\"para\">\r\n					企业实施知识管理的原因在于：\r\n				</div>\r\n				<div class=\"anchor-list\">\r\n					<a name=\"undefined\"></a> <a name=\"undefined\"></a> <a name=\"undefined\"></a> \r\n				</div>\r\n				<h3 class=\"para-title level-3\">\r\n					<span class=\"title-text\">竞争</span> \r\n				</h3>\r\n				<div class=\"para\">\r\n					市场竞争越来越激烈，创新的速度加快，所以企业必须不断获得新知识，并利用知识为企业和社会创造价值；\r\n				</div>\r\n				<div class=\"anchor-list\">\r\n					<a name=\"undefined\"></a> <a name=\"undefined\"></a> <a name=\"undefined\"></a> \r\n				</div>\r\n				<h3 class=\"para-title level-3\">\r\n					<span class=\"title-text\">顾客导向</span> \r\n				</h3>\r\n				<div class=\"para\">\r\n					企业要为客户创造价值；\r\n				</div>\r\n				<div class=\"anchor-list\">\r\n					<a name=\"undefined\"></a> <a name=\"undefined\"></a> <a name=\"undefined\"></a> \r\n				</div>\r\n				<h3 class=\"para-title level-3\">\r\n					<span class=\"title-text\">工作流动</span> \r\n				</h3>\r\n				<div class=\"para\">\r\n					雇员的流动性加快，雇员倾向于提前退休，如果企业不能很好地管理其所获得的知识，企业有失去其知识基础的风险；\r\n				</div>\r\n				<div class=\"anchor-list\">\r\n					<a name=\"undefined\"></a> <a name=\"undefined\"></a> <a name=\"undefined\"></a> \r\n				</div>\r\n				<h3 class=\"para-title level-3\">\r\n					<span class=\"title-text\">不确定性</span> \r\n				</h3>\r\n				<div class=\"para\">\r\n					环境的不确定性表现在由于竞争而导致的不确定性和由于模糊性而带来的不确定性，在动态的不确定环境下，技术更新速度加快，学习已成为企业得以生存的根本保证，组织成员获取知识和使用知识的能力成为组织的核心技能，知识已成为企业获取竞争优势的基础，成为企业重要的稀缺资产；\r\n				</div>\r\n				<div class=\"anchor-list\">\r\n					<a name=\"undefined\"></a> <a name=\"undefined\"></a> <a name=\"undefined\"></a> \r\n				</div>\r\n				<h3 class=\"para-title level-3\">\r\n					<span class=\"title-text\">影响</span> \r\n				</h3>\r\n				<div class=\"para\">\r\n					全球化经营要求企业具有交流沟通能力以及知识获取、知识创造与知识转换的能力。知识创造、知识获取和知识转换依赖于企业的学习能力，学习是企业加强竞争优势和核心竞争力的关键。\r\n				</div>\r\n				<div class=\"anchor-list\">\r\n					<a name=\"undefined\"></a> <a name=\"undefined\"></a> <a name=\"undefined\"></a> \r\n				</div>\r\n				<h2 class=\"para-title level-2\">\r\n					<span class=\"title-text\">评估方法</span> <a class=\"edit-icon j-edit-link\"><em class=\"cmn-icon wiki-lemma-icons wiki-lemma-icons_edit-lemma\"></em>编辑</a> \r\n				</h2>\r\n				<div class=\"para\">\r\n					可以从以下几个方面评估知识管理的实施效果：\r\n				</div>\r\n				<div class=\"para\">\r\n					1.人力资本 培训费用 组织学习 员工忠诚度 管理经验\r\n				</div>\r\n				<div class=\"para\">\r\n					2. <a target=\"_blank\">创新资本</a> 研发费用 从事创新的员工比率 产品更新 知识产权\r\n				</div>\r\n				<div class=\"para\">\r\n					3. <a target=\"_blank\">客户资本</a> 满意度 服务质量 合作的时间 重复购买 销售额\r\n				</div>\r\n				<div class=\"para\">\r\n					4.知识识别阶段 知识库中联系的数目 知识库中主题的数目 点击率 生产力\r\n				</div>\r\n				<div class=\"para\">\r\n					5.知识诱导阶段 来自于知识库的新需求 可达到的相关资源\r\n				</div>\r\n				<div class=\"para\">\r\n					6.知识分发阶段 推的方式分发 拉的方式分发\r\n				</div>\r\n				<div class=\"para\">\r\n					7.知识利用阶段 知识聚集活动中的系统利用率 用户满意度 知识利用过程中产生的商业机会\r\n				</div>\r\n				<div class=\"anchor-list\">\r\n					<a name=\"undefined\"></a> <a name=\"undefined\"></a> <a name=\"undefined\"></a> \r\n				</div>\r\n				<h2 class=\"para-title level-2\">\r\n					<span class=\"title-text\">关系</span> <a class=\"edit-icon j-edit-link\"><em class=\"cmn-icon wiki-lemma-icons wiki-lemma-icons_edit-lemma\"></em>编辑</a> \r\n				</h2>\r\n				<div class=\"para\">\r\n					知识管理和内容管理究竟是什么关系呢？它们有什么样的区别和联系呢？我想我们有必要对两者作一个比较。让企业对二者有个清楚的认识，帮助企业选择到底是该知识管理还是内容管理，以及什么时候实施知识管理和内容管理。\r\n				</div>\r\n<br />\r\n				<h3 class=\"para-title level-3\">\r\n					<span class=\"title-text\">关注对象</span> \r\n				</h3>\r\n				<div class=\"para\">\r\n					1、知识管理和内容管理的关注的对象不同\r\n				</div>\r\n				<div class=\"para\">\r\n					针对知识管理和内容管理的具体概念，纵说纷纭。我们没有必要拘泥于知识管理和内容管理的概念到底是什么，但是从知识管理和内容管理关注的对象上还是可以看出它们的不同（如图一所示）。知识管理关注的是对企业内外部的显性、隐性知识的管理，显性知识包括内外部的研究报告、标准规范、程序文档和数据等；而隐性知识包括隐藏在人的大脑中的经验，和隐含在企业业务中还没有被发现的知识或经验。内容管理中的\"内容\"实质上就是任何类型的数字信息的结合体，可以是文本、图形图象、Web页面、业务文档、数据库表单、视频、声音文件等。从这点上看内容管理主要是对显性知识的管理。同时知识管理还存在对知识活动的管理，即知识沉淀、共享、应用学习、创新等环节的管理，由此就会延伸到对人的知识行为的管理--管理制度、企业文化等方面。而内容管理只是针对静态的显性知识的一种管理，将分散混乱的数据、信息转化成有组织的内容和知识，实现知识的关联化。\r\n				</div>\r\n				<div class=\"anchor-list\">\r\n					<a name=\"undefined\"></a> <a name=\"undefined\"></a> <a name=\"undefined\"></a> \r\n				</div>\r\n				<h3 class=\"para-title level-3\">\r\n					<span class=\"title-text\">实践主体</span> \r\n				</h3>\r\n				<div class=\"para\">\r\n					2、知识管理和内容管理目前的实践主体不同\r\n				</div>\r\n				<div class=\"para\">\r\n					从目前知识管理和内容管理实践的企业或机构来说，它们还是存在很大的差异性。在内容管理领域，目前主要是政府、媒体、事业单位等，就实施的内容而言主要还是网站内容的管理；而知识管理主要还是面对企业，在寻求与企业现有管理架构，IT应用系统结合方面实现企业价值增值，在具体实施方面重点关注企业知识的梳理，知识和人的关联，人与人的关联以及和企业业务的融合。\r\n				</div>\r\n				<div class=\"anchor-list\">\r\n					<a name=\"undefined\"></a> <a name=\"undefined\"></a> <a name=\"undefined\"></a> \r\n				</div>\r\n				<h3 class=\"para-title level-3\">\r\n					<span class=\"title-text\">过程目的</span> \r\n				</h3>\r\n				<div class=\"para\">\r\n					3、过程目的--必然的融合\r\n				</div>\r\n				<div class=\"para\">\r\n					根据IDC研究报告指出，知识管理是未来企业提高工作效率和增加竞争力的关键。作为其不可或缺的核心基础--企业内容管理方案，便成为业界炙手可热的新议题。从上面的分析也可以看出，内容管理和知识管理并不是独立存在的，内容管理的目的还是为了达到知识管理，内容管理和知识管理好比一个是过程，一个是目的。在目前，内容管理和知识管理在各自关注的领域独立发展是有好处的，随着发展的深入，必然交叉融合，最后内容管理就会真正成为知识管理的一部分。\r\n				</div>\r\n				<div class=\"anchor-list\">\r\n					<a name=\"undefined\"></a> <a name=\"undefined\"></a> <a name=\"undefined\"></a> \r\n				</div>\r\n				<h2 class=\"para-title level-2\">\r\n					<span class=\"title-text\">联系实际</span> <a class=\"edit-icon j-edit-link\"><em class=\"cmn-icon wiki-lemma-icons wiki-lemma-icons_edit-lemma\"></em>编辑</a> \r\n				</h2>\r\n				<div class=\"para\">\r\n					知识管理并不应被狭义地看待。它涉及了许多的相关研究领域。诸如它可以和学习、创新、教育、记忆、文化、人力资源管理、 <a target=\"_blank\">心理科学</a>、脑科学、管理科学、信息科学、信息技术、 <a target=\"_blank\">图书馆学</a>和 <a target=\"_blank\">情报学</a>等联系在一起。它并不单纯是一种 <a target=\"_blank\">管理理论</a>，而是涉及到从技术到管理再到哲学的多个层面。因此，通过任何一个简单的框架或模型，也许能理解知识管理的基本涵义，但是却远远不能涵盖其全部意义，而且也无此必要。随着时代的发展开始使用知识管理软件是对知识进行信息化处理。以佳盟个人信息管理软件为例，知识管理软件是通过软件对知识进行系统、全面、分类的管理，以便查找和使用。\r\n				</div>\r\n				<div class=\"anchor-list\">\r\n					<a name=\"undefined\"></a> <a name=\"undefined\"></a> <a name=\"undefined\"></a> \r\n				</div>\r\n				<h2 class=\"para-title level-2\">\r\n					<span class=\"title-text\">作用</span> <a class=\"edit-icon j-edit-link\"><em class=\"cmn-icon wiki-lemma-icons wiki-lemma-icons_edit-lemma\"></em>编辑</a> \r\n				</h2>\r\n				<div class=\"para\">\r\n					三点：\r\n				</div>\r\n				<div class=\"para\">\r\n					1.提高 <a target=\"_blank\">组织智商</a>；\r\n				</div>\r\n				<div class=\"para\">\r\n					2.提升 <a target=\"_blank\">组织记忆</a>；\r\n				</div>\r\n				<div class=\"para\">\r\n					3.减少重复劳动\r\n				</div>\r\n				<div class=\"para\">\r\n					把知识积累起来！\r\n				</div>\r\n				<div class=\"para\">\r\n					构建 <a target=\"_blank\">企业知识库</a>，对纷杂的知识内容（方案、策划、制度等）和格式（图片、word、excel、ppt、pdf等）\r\n				</div>\r\n				<div class=\"para\">\r\n					分门别类管理。\r\n				</div>\r\n				<div class=\"para\">\r\n					充分发动每个部门、员工，贡献自己所掌握的企业知识，积少成多， <a target=\"_blank\">聚沙成塔</a>。\r\n				</div>\r\n				<div class=\"para\">\r\n					重视企业原有知识数据，进行批量导入，纳入管理范畴。\r\n				</div>\r\n				<div class=\"para\">\r\n					帮助企业评估 <a target=\"_blank\">知识资产</a>量、使用率、增长率。\r\n				</div>\r\n				<div class=\"para\">\r\n					把知识管理起来！\r\n				</div>\r\n				<div class=\"para\">\r\n					创建企业 <a target=\"_blank\">知识地图</a>，清晰了解企业知识分布状况，提供 <a target=\"_blank\">管理决策</a>依据。\r\n				</div>\r\n				<div class=\"para\">\r\n					构建知识权限体系，对不同角色的员工开放不同级别的知识库，保证企业知识安全。\r\n				</div>\r\n				<div class=\"para\">\r\n					注重版本管理，文件资料从初稿到最后一版，均有版本记录保存并可查。\r\n				</div>\r\n				<div class=\"para\">\r\n					把知识应用起来！\r\n				</div>\r\n				<div class=\"para\">\r\n					让知识查询调用更加简单，充分利用知识成果，提供工作效率，减少重复劳动。\r\n				</div>\r\n				<div class=\"para\">\r\n					依据知识库构建各部门各岗位的学习培训计划，随时自我充电 ，成为“ <a target=\"_blank\">学习型团队</a>”。\r\n				</div>\r\n				<div class=\"para\">\r\n					提供知识问答模式，将一些知识库中缺少的经验性知识，从员工头脑中挖掘出来。\r\n				</div>\r\n				<div class=\"para\">\r\n					支持异地 <a target=\"_blank\">协同</a>，通过互联网获取知识库内容，为异地办公提供知识支持。\r\n				</div>\r\n				<div class=\"anchor-list\">\r\n					<a name=\"undefined\"></a> <a name=\"undefined\"></a> <a name=\"undefined\"></a> \r\n				</div>\r\n				<h2 class=\"para-title level-2\">\r\n					<span class=\"title-text\">步骤</span> <a class=\"edit-icon j-edit-link\"><em class=\"cmn-icon wiki-lemma-icons wiki-lemma-icons_edit-lemma\"></em>编辑</a> \r\n				</h2>\r\n				<div class=\"anchor-list\">\r\n					<a name=\"undefined\"></a> <a name=\"undefined\"></a> <a name=\"undefined\"></a> \r\n				</div>\r\n				<h3 class=\"para-title level-3\">\r\n					<span class=\"title-text\">认知</span> \r\n				</h3>\r\n				<div class=\"para\">\r\n					第一步：认知\r\n				</div>\r\n				<div class=\"para\">\r\n					认知是企业实施知识管理的第一步，主要任务是统一企业对知识管理的认知，梳理知识管理对企业管理的意义，评估企业的知识管理现状。帮助企业认识是否需要知识管理，并确定知识管理实施的正确方向。主要工作包括：全面完整的认识知识管理，对企业中高层进行知识管理认知培训，特别是让企业高层认识知识管理；利用 <a target=\"_blank\">知识管理成熟度模型</a>等评价工具多方位评估企业知识管理现状及通过调研分析企业管理的主要问题；评估知识管理为企业带来的长、短期效果；从而为是否推进知识管理实践提供决策支持；制定 <a target=\"_blank\">知识管理战略</a>和推进方向等。\r\n				</div>\r\n				<div class=\"anchor-list\">\r\n					<a name=\"undefined\"></a> <a name=\"undefined\"></a> <a name=\"undefined\"></a> \r\n				</div>\r\n				<h3 class=\"para-title level-3\">\r\n					<span class=\"title-text\">规划</span> \r\n				</h3>\r\n				<div class=\"para\">\r\n					第二步：规划\r\n				</div>\r\n				<div class=\"para\">\r\n					知识管理的推进是一套系统工程，在充分认知企业需求的基础上，详细规划也是确保知识管理实施效果的重要环节。这个环节主要是通过对知识管理现状、知识类型的详细分析，并结合业务流程等多角度，进行知识管理规划。在规划中，切记知识管理只是过程，而不能为了知识管理而进行知识管理，把知识管理充分溶入企业管理之中，才能充分发挥知识管理的实施效果。主要工作包括：从战略、业务流程及岗位来进行知识管理规划；企业管理现状与知识管理发展的真实性分析；制订知识管理相关战略目标和实施策略，并对流程进行合理化改造；知识管理落地的需求分析及规划；在企业全面建立知识管理的理论基础。\r\n				</div>\r\n				<div class=\"anchor-list\">\r\n					<a name=\"undefined\"></a> <a name=\"undefined\"></a> <a name=\"undefined\"></a> \r\n				</div>\r\n				<h3 class=\"para-title level-3\">\r\n					<span class=\"title-text\">试点</span> \r\n				</h3>\r\n				<div class=\"para\">\r\n					第三步：试点\r\n				</div>\r\n				<div class=\"para\">\r\n					此阶段是第二阶段的延续和实践，按照规划选取适当的部门和流程依照规划基础进行知识管理实践。并从短期效果来评估知识管理规划，同时结合试点中出现的问题进行修正。主要工作内容：每个企业都有不同的业务体系，包括：生产、研发、销售等，各不同业务体系的任务特性均不相同，其完成任务所需要的知识亦有不同，因此需要根据不同业务体系的任务特性和知识应用特点，拟订最合适、成本最低的知识管理方法，这称为知识管理模式分析KMPA。另外，考虑到一种业务体系下有多方面的知识，如何识别关键知识，并判断关键知识的现状，进而在KM模式的指导下采取有针对性的提升行为，这可以称为知识管理策略规划 <a target=\"_blank\">KSP</a>。所以，此阶段的重点是结合企业业务模式进行知识体系梳理，并对知识梳理结果进行分析，以确定知识管理具体策略和提升行为。本阶段是知识管理从战略规划到落地实施的阶段，根据对企业试点部门的知识管理现状、需求和提升计划的分析，应该考虑引入支撑知识管理落地的知识管理IT系统。根据前几个阶段的规划和分析，选择适合企业现状的IT落地方法，如带知识管理功能的办公 <a target=\"_blank\">协同系统</a>、 <a target=\"_blank\">知识管理系统</a>、知识门户落地等等。可以说，本阶段在知识管理系统实施中难度最大，需要建立强有力的项目保障团队，做好 <a target=\"_blank\">业务部门</a>、咨询公司、系统开发商等多方面协调工作。\r\n				</div>\r\n				<div class=\"anchor-list\">\r\n					<a name=\"undefined\"></a> <a name=\"undefined\"></a> <a name=\"undefined\"></a> \r\n				</div>\r\n				<h3 class=\"para-title level-3\">\r\n					<span class=\"title-text\">推广</span> \r\n				</h3>\r\n				<div class=\"para\">\r\n					第四步：推广和支持\r\n				</div>\r\n				<div class=\"para\">\r\n					在试点阶段不断修正知识管理规划的基础上，知识管理将大规模在企业推广，以全面实现其价值。推广内容：知识管理试点部门的实践，在企业中其他部门的复制；知识管理全面的溶入企业业务流程和价值链；知识管理制度初步建立；知识管理系统的全面运用；实现社区，学习型组织、头脑风暴等知识管理提升计划的全面运行，并将其制度化。\r\n				</div>\r\n				<div class=\"para\">\r\n					难点：对全面推广造成的混乱进行控制和对知识管理实施全局的把握；知识管理融入业务流程和日常工作；文化、管理、技术的协调发展；知识管理对战略目标的支持；对诸如思想观念转变等人为因素的控制以及利益再分配；建立知识管理的有效激励机制和绩效体系。\r\n				</div>\r\n				<div class=\"anchor-list\">\r\n					<a name=\"undefined\"></a> <a name=\"undefined\"></a> <a name=\"undefined\"></a> \r\n				</div>\r\n				<h3 class=\"para-title level-3\">\r\n					<span class=\"title-text\">制度化</span> \r\n				</h3>\r\n				<div class=\"para\">\r\n					第五步：制度化\r\n				</div>\r\n				<div class=\"para\">\r\n					制度化阶段既是知识管理项目实施的结束，又是企业知识管理的一个新开端，同时也是一个自我完善的过程。要完成这一阶段，企业必须重新定义战略，并进行组织构架及 <a target=\"_blank\">业务流程</a>的重组，准确评估知识管理在企业中实现的价值。\r\n				</div>\r\n				<div class=\"para\">\r\n					<b>知识管理的内容：<sup>[1]</sup><a name=\"undefined\"></a> </b> \r\n				</div>\r\n				<div class=\"para\">\r\n					1) 隐性知识显性化：\r\n				</div>\r\n				<div class=\"para\">\r\n					A. 企业面临的问题：有权威机构研究表明，对大多数企业来说结构化、系统化且可供员工参考的知识信息只占到企业知识总量的10%，其他90%的知识都存在于员工个人大脑中难于数据化和系统化的应用于企业全员层面。这对企业来说无疑是巨大的知识浪费。\r\n				</div>\r\n				<div class=\"para\">\r\n					2) 信息文档管理：\r\n				</div>\r\n				<div class=\"para\">\r\n					A. 企业面临的问题：以往企业由于在日常管理及工作过程中缺乏知识管理理念和有针对性的信息文档管理系统，导致员工在查找知识信息文档时浪费过多时间，甚至查找不到需要的文件。致使工作效率降低及企业资源浪费。\r\n				</div>\r\n				<div class=\"para\">\r\n					3) 知识共享与循环应用：\r\n				</div>\r\n				<div class=\"para\">\r\n					A. 企业面临的问题：当前很多企业认为只要引入了知识管理项目、上了知识管理信息平台公司就会自动向知识管理导向转型、公司潜在的隐性知识就会自动转化为竞争优势，其结果往往都是差强人意。其根本原因在于忽视了知识管理的本质驱动：流程融入与企业文化。\r\n				</div>\r\n				<div class=\"para\">\r\n					4) 企业知识资产安全管理：\r\n				</div>\r\n				<div class=\"para\">\r\n					A. 企业面临的问题：企业对在日常管理运作当中产生的各项知识成果与文件如营销方案、客户资料、财务报告、产品配方、生产工艺、设计图纸、货源情况等对企业可以形成核心竞争优势的重要资源若不能及时有效的进行保密安全管理，一旦发生知识外泄将会对企业带来不可估量的负面影响。因此良好的知识资产安全管理可以起到降低运营风险、保持企业竞争力的作用。\r\n				</div>\r\n				<div class=\"anchor-list\">\r\n					<a name=\"undefined\"></a> <a name=\"undefined\"></a> <a name=\"undefined\"></a> \r\n				</div>\r\n				<h2 class=\"para-title level-2\">\r\n					<span class=\"title-text\">管理知识</span> <a class=\"edit-icon j-edit-link\"><em class=\"cmn-icon wiki-lemma-icons wiki-lemma-icons_edit-lemma\"></em>编辑</a> \r\n				</h2>\r\n				<div class=\"anchor-list\">\r\n					<a name=\"undefined\"></a> <a name=\"undefined\"></a> <a name=\"undefined\"></a> \r\n				</div>\r\n				<h3 class=\"para-title level-3\">\r\n					<span class=\"title-text\">实际应用</span> \r\n				</h3>\r\n				<div class=\"para\">\r\n					<div class=\"lemma-picture text-pic layout-right\">\r\n						<a class=\"image-link\" target=\"_blank\"></a><span class=\"description\"><br />\r\n</span> \r\n					</div>\r\n知识管理作为一种管理理念，是需要长期坚持并且努力形成文化的活动。从这个概念上来讲，很明确，知识管理不是项目，因此也就不能按照项目管理的组织和运作方式来进行管理。但这并不是说项目管理的思想和方法不能运用在知识管理中，随着项目管理的意义被更多的人所认可，项目管理中的一些思想也已经慢慢演变为普遍适用的管理理念，这些思想也同样适用于知识管理。\r\n				</div>\r\n				<div class=\"para\">\r\n					知识管理的目标，我们可以列举很多，比如在合适的时间把合适的知识传递给合适的人，提高组织的竞争力，促进组织创新，保护组织的知识资产，避免知识随着人才的流失而流失，获取更多的商业利益等等。当你的组织确定要实施KM的时候，可能是为了获得前面罗列的这些好处的全部或者某一部分，但是它们却不能作为指导具体知识管理实践活动的目标。因为前面所有的这些说法，都不符合项目管理中目标的 <a target=\"_blank\">SMART原则</a>，就是\r\n				</div>\r\n				<div class=\"para\">\r\n					S-Specific明确的陈述\r\n				</div>\r\n				<div class=\"para\">\r\n					M-Measurable可以衡量的结果\r\n				</div>\r\n				<div class=\"para\">\r\n					A-Attainable可以达成的目标\r\n				</div>\r\n				<div class=\"para\">\r\n					R-Realistic合理，实在 或者说是能和实际工作相结合\r\n				</div>\r\n				<div class=\"para\">\r\n					T-Trackable可以跟踪的\r\n				</div>\r\n				<div class=\"para\">\r\n					什么样的知识管理目标才符合SMART原则呢？例如提高组织竞争力，可以按照明确和可衡量的原则具体化为市场占有率，用户认可率，利润的提高等等。但是这些指标是受多种因素影响的，并不能准确确定哪些是由知识管理带来的，而且这一目标对知识管理实践活动的针对性和指导性也不强，不符合R和T的原则，因此它不是一个理想的目标。知识管理作为一种管理理念，需要通过具体的活动来落实，理想的目标也应该是和具体的活动想结合的。就象一个人说自己经常参加体育活动，他可能是经常去健身房，也可能是经常跑步或者游泳打球等等。不管他干什么，他的体育活动一定是通过某项特定的活动体现出来的。他参加体育活动是为了身体健康，但是健康却不是一个好的目标，用身体健康来指导自己锻炼的人多数情况下可能会流于形式，真正的目标必须和具体的活动结合，比如每天坚持健身30分钟，跑步5000米等。知识管理也是这样，讨论研究的时候可以说知识管理如何如何，到了真正要实施的时候，就不能只是谈谈文化、理念什么的了，必须落实到具体确定的活动上，如IBM有e-workplace、HP有connex、HP中国有读书会（虽然近期 <a target=\"_blank\">高建华</a>的离职使读书会的作用受到了一些质疑）、西门子有shareNet、 <a target=\"_blank\">埃森哲</a>有Knowledge Xchange等等。符合SMART原则的知识管理目标也应该和具体的活动相结合，针对具体的活动来设定，前面我们罗列的目标其实更适合作为Vision而不是目标。因此，一个组织开始实施知识管理的过程可以是这样的，定义自己的Vision，就是你最希望通过知识管理解决的问题；选择合适的适合自身情况的一种或几种具体活动；为这些活动制定一个符合SMART原则的目标；然后开始PDCA的过程，就是计划、执行、检查、行动。\r\n				</div>\r\n				<div class=\"para\">\r\n					<a target=\"_blank\">战略项目管理</a>概述\r\n				</div>\r\n				<div class=\"para\">\r\n					企业战略项目管理是项目管理的新理念，它是服务于企业战略的 <a target=\"_blank\">项目管理方法</a>，并要求企业从高层到基层每位员工的参与，在全方位的项目管理信息系统支持下，利用 <a target=\"_blank\">系统思维方法</a>去解决企业范围内的项目管理问题，使企业战略项目管理的理念、方法等融人到企业文化之中。\r\n				</div>\r\n				<div class=\"para\">\r\n					战略项目管理的提出\r\n				</div>\r\n				<div class=\"anchor-list\">\r\n					<a name=\"undefined\"></a> <a name=\"undefined\"></a> <a name=\"undefined\"></a> \r\n				</div>\r\n				<h3 class=\"para-title level-3\">\r\n					<span class=\"title-text\">实践背景</span> \r\n				</h3>\r\n				<div class=\"para\">\r\n					从实践角度看，目前中国大多数企业的项目管理现状不容乐观，在对诸多企业的实地调查发现，中国企业现在的项目管理活动具有以下特点：没有专门的人员或机构进行企业项目管理活动，每个人或部门往往是针对自己工作中的任务 （项目）独立地采取一定对策，缺乏系统性、全局性。更有一些企业及部门根本就没有项目及项目管理的概念及意识；企业中的项目管理基本上是一种被动式管理，常见现象是临时性、一次性的应付活动；企业中项目管理活动往往是瞬时或间断性的，事后则无相应的总结积累；缺乏系统、科学的企业整体发展战略层面的 <a target=\"_blank\">项目管理理论</a>方法指导。因此，相对于项目管理生命周期模块、职能领域模块以及方法与工具模块，作为项目管理公用知识模块重要内容的企业项目管理在中国的发展尚处于起步阶段，是中国项目管理研究的薄弱环节，同时又是急待加强的环节。\r\n				</div>\r\n				<div class=\"para\">\r\n					而实践中西方几乎所有的大企业都有专门从事项目管理的部门及项目管理者，且中小企业也通过不同形式与项目管理咨询公司或专家合作对该企业的项目加以管理。如Flnorz公司董事长在总结自己20年来在项目环境中运行的体会时指出：“当项目经理拥有最大的权利和责任时，任务团队概念是实现项目目标的最有效方法；尽管Fluorz公司建立了基本的项目管理原则，但它并没有设计出单一的标准项目组织和项目程序来很好地应用于更多的项目。”至上世纪末期，美国众多企业最终认识到战略管理和项目管理两者之间的关系以及他们的重要性，从单纯强调战略制定转为制定和实施并重，认识到项目管理原则既可应用到运作性计划实施当中，也可应用到战略计划的实施当中。这些实践活动使他们认识到企业项目管理不应孤立地进行若干个独立项目的管理，企业项目管理活动的有效性都要从企业整体角度来衡量，企业正日益强烈地意识到从系统角度进行战略项目管理活动的重要性。\r\n				</div>\r\n				<div class=\"anchor-list\">\r\n					<br />\r\n				</div>\r\n				<h3 class=\"para-title level-3\">\r\n					<span class=\"title-text\">理论背景</span> \r\n				</h3>\r\n				<div class=\"para\">\r\n					从理论角度研究，尽管广义上讲人类的历史也可以说是项目管理的历史，然而真正提出“项目”一词并对之进行研究也不过开始于上世纪四、五十年代。美国“曼哈顿计划”中实施项目管理取得的巨大成功，引起了理论界的广泛关注，此后对项目管理的研究逐步趋向系统化、专门化，并使项目管理最终成为企业管理中一门独立的学科，并广泛应用于IT、金融、服务以及工程等诸多行业。项目管理在思维方式、运作方法和管理模式上最大限度地利用了组织的内部和外部资源，从根本上改善了管理人员的工作流程，并使运营效率得到大幅度提升。虽然项目管理至今已有许多成熟理论成果，但在高度不确定环境下的企业项目管理方面，国外的研究也不过是20余年的历史。\r\n				</div>\r\n				<div class=\"para\">\r\n					自20世纪80年代开始，随着项目管理理论在西方发达国家取得突破，其应用范围也越来越广泛，从传统的“工程项目”扩展到各行各业广泛的“一次性任务”。虽然当时“一次性任务”成为项目管理的对象，但它与长期性组织（是区别于“项目”的临时性组织而言的，如企业或政府部门）之间存在着不协调的因素，由于对 “单个项目”的项目管理方法所关注的重点是该“单个项目”自身目标的实现，因而在同一组织背景下开展多个项目的时候就不可避免各种冲突的发生。各个“单个项目”追求自身目标的实现，结果可能是部分“单个项目”的目标虽然实现了，而整个长期性组织的目标却未能实现；甚或连“单个项目”的自身目标都由于各个项目间的相互牵制而无法实现。可见缺乏项目管理运作平台的长期性组织，无法有效实施项目管理，项目管理生存的要求呼唤企业项目管理的出现。但长期性组织对要对项目管理提供支持，需要建立一套与项目管理相适应的 <a target=\"_blank\">组织管理体系</a>，以使长期性组织能够提供项目管理所要求的“临时性”和“柔性”需求，这就要求在企业范围内提供项目管理的有效实施平台。企业项目管理就是伴随着项目管理在长期性组织（如企业或政府部门等）中的广泛应用而逐步形成的一种以长期性组织为对象的管理方法和模式。\r\n				</div>\r\n				<div class=\"para\">\r\n					企业项目管理早期的概念是基于项目型公司而提出来的，是指“管理整个企业范围内的项目”，即着眼于企业层次总体战略目标的实现对企业中的诸多项目实施管理。如Microsoft研究人员提出了EPM解决方案，特别为企业集中管理和分享项目信息而设计。该解决方案可实现三类目标：内部标准化的项目与资源信息集中储存，以实现高效的项目信息分享、分析与管理；决策层可利用它获得关于全公司的活动及相应状态的宽广视图，从而进行项目组合分析和风险管理并做出合理的运营决策；项目经理和项目组成员以及项目合作伙伴可以通过基于Web的协作界面创建项目计划并对项目进行跟踪、报告、分析和控制。\r\n				</div>\r\n				<div class=\"para\">\r\n					随着外部环境的发展变化，项目管理方法在长期性组织中的广泛应用已不局限于传统的“项目型公司”，传统的生产运作型企业及政府部门等非企业型组织中也广泛地实施项目管理。随着国际项目管理研究对企业项目管理研究的越来越重视，企业项目管理逐渐形成自己的体系，即包括“multi project management”、“portfolio management”、“program management”三个子系统。至此，企业项目管理已成为一种长期性组织（不局限于企业组织）管理方式地代名词。企业项目管理的实质逐渐显示为一种以 “项目”为中心的长期性组织管理方式，其主导思想是“按项目进行管理”（management by project），其核心是基于项目管理的组织管理体系，它使长期性组织的管理由面向“职能”、面向“过程”的管理转变为面向“对象”的管理，成为国际项目管理研究的一个重要领域。\r\n				</div>\r\n				<div class=\"para\">\r\n					2002年6月在德国柏林IPMA全球会议上，企业项目管理（EPM）更作为会议六大研讨主题之一。英国、澳大利亚、 <a target=\"_blank\">奥地利</a>、德国、西班牙等地的项目管理专家分别就multi project management和portfolio management领域的研究进行了交流，充分显示企业项目管理作为国际项目管理研究重要领域的地位。\r\n				</div>\r\n				<div class=\"para\">\r\n					因此，迄今为止理论界运用多学科如系统工程、 <a target=\"_blank\">技术经济分析</a>、模糊数学、控制论、计算机科学、心理学等自然科学和社会科学理论知识，已经提出了较为系统的项目计划、进度和控制的理论方法，然而系统的项目管理理论还集中在项目层面，针对具体项目的孤立分析及采取应对策略。但学者们也从不同角度开始了对企业不同层面项目的“集成管理”研究，提出了项目成组管理等理论，并在IT行业进行了有益的探索，逐步发展着企业层面的项目管理理论。对“基于战略视角”企业项目管理模式的研究尚未提出相应的系统集成理论方法。也就是说，在企业项目管理迅猛发展的背景下，恰恰对于企业战略项目管理的研究颇为缺乏，迫切需要科学的理论方法指导该模式下的项目管理活动。\r\n				</div>\r\n<span class=\"title\"></span>&nbsp;\r\n			</div>\r\n		</div>\r\n	</div>\r\n</div>', null, null, '402888ac506f5b7401506f9724bd003f', '20151016153947', '20151016153947', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', 'CREAT', '1', null);
INSERT INTO `farm_doctext` VALUES ('<p>\r\n	本项目的应用场景是管理业务团队的相关知识（API、代码片段、知识定义、技术经验...） 但是其应用并不局限于这些应用。其实这就是一个 知识库、知识管理系统、以及若干基于知识的工具和视图。\r\n</p>\r\n<p>\r\n	<br />\r\n</p>\r\n<h1>\r\n	知识发布、知识展示<br />\r\n</h1>\r\n<ul>\r\n	<li>\r\n		<p>\r\n			历史版本查看\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<p>\r\n			目录章节展示\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<p>\r\n			源代码展示\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<p>\r\n			文档好评差评操作\r\n		</p>\r\n	</li>\r\n	<li>\r\n		知识评论<br />\r\n	</li>\r\n	<li>\r\n		<p>\r\n			全文检索\r\n		</p>\r\n	</li>\r\n	<li>\r\n		上传附件资源\r\n	</li>\r\n	<li>\r\n		热度计算\r\n	</li>\r\n	<li>\r\n		权限控制\r\n	</li>\r\n	<li>\r\n		关联知识推送<br />\r\n	</li>\r\n</ul>\r\n<p>\r\n	<br />\r\n</p>\r\n<h1>\r\n	知识小组<br />\r\n</h1>\r\n<ul>\r\n	<li>\r\n		<p>\r\n			支持整站下载\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<p>\r\n			单个网页下载\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<p>\r\n			对下载内容通过配置的方式抽取有用数据\r\n		</p>\r\n	</li>\r\n</ul>\r\n<h1>\r\n	权限划分<br />\r\n</h1>\r\n<ul>\r\n	<li>\r\n		<p>\r\n			文档访问权限和编辑权限可以分别控制：\r\n		</p>\r\n		<p>\r\n			[高级权限|普通权限] &nbsp;X &nbsp;[小组|创建人|公开] &nbsp;X &nbsp;[系统中N个小组] &nbsp;X &nbsp;[阅读|编辑]\r\n		</p>\r\n	</li>\r\n</ul>\r\n<h1>\r\n	统计分析<br />\r\n</h1>\r\n<ul>\r\n	<li>\r\n		<p>\r\n			用户好评度\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<p>\r\n			小组好评度\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<p>\r\n			用户发布量\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<p>\r\n			文章好评\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<p>\r\n			文章差评\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<p>\r\n			系统日使用量\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<p>\r\n			月使用量\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<p>\r\n			年使用量\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<p>\r\n			文档好评率\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<p>\r\n			文档访问量\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<p>\r\n			用户累计好评\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<p>\r\n			用户累计差评\r\n		</p>\r\n	</li>\r\n	<li>\r\n		<p>\r\n			用户等级等统计项\r\n		</p>\r\n	</li>\r\n</ul>\r\n<h1>\r\n	<strong>WCP3版本说明</strong>\r\n</h1>\r\n<ul>\r\n	<li>\r\n		使用springMVC替换struts2\r\n	</li>\r\n	<li>\r\n		开源farm_Lucene插件\r\n	</li>\r\n	<li>\r\n		代码50%以上重构\r\n	</li>\r\n	<li>\r\n		去除web站点发布功能\r\n	</li>\r\n	<li>\r\n		去除爬虫功能\r\n	</li>\r\n	<li>\r\n		个别页面UI有重新设计\r\n	</li>\r\n	<li>\r\n		去掉计划任务功能<br />\r\n	</li>\r\n</ul>', null, null, '402888ac5070201401507020f777000f', '20151016152433', '20151016152433', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '修改内容', '2', '402888ac506eaf5b01506ebfe7aa000b');
INSERT INTO `farm_doctext` VALUES ('&nbsp;&nbsp;&nbsp;&nbsp;WCP当前版本（开源版）允许任何人进行自由下载和自由传播以及使用，但是未经作者同意禁止任何人通过任何手段和名目向使用着索取费用。<br />\r\n<h1>\r\n	&nbsp;&nbsp;&nbsp;&nbsp;请谨慎使用\r\n</h1>\r\n<p>\r\n	&nbsp;&nbsp;&nbsp;&nbsp;由于当前版本的测试规模以及本人的测试能力限制，在本系统（WCP2标准版）使用过程中可能会发生未知的运行错误，这些错误可能会导致本软件中的数据问题，最坏情况下可能会丢失已经维护进入系统的数据。\r\n</p>\r\n<h1>\r\n	&nbsp;&nbsp;&nbsp;&nbsp;提示\r\n</h1>\r\n&nbsp;&nbsp;&nbsp;&nbsp;通过有限的测试，可以预料在本软件的安装和使用过程中，不会损坏到操作系统中其它部分的数据和功能（特殊情况未经验证，所以还是会有一定风险）。<br />\r\n如果您在使用过程中遇到难以解决的问题或者是使用中的疑惑，可以通过系统中的联系方式获得帮助。<br />', null, null, '402888ac507030790150704fae7e002b', '20151016153141', '20151016153141', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', 'CREAT', '2', '402888ac506f5b7401506f8fb8860021');

-- ----------------------------
-- Table structure for `farm_doctype`
-- ----------------------------
DROP TABLE IF EXISTS `farm_doctype`;
CREATE TABLE `farm_doctype` (
  `NAME` varchar(128) NOT NULL,
  `TYPEMOD` varchar(128) DEFAULT NULL,
  `CONTENTMOD` varchar(128) DEFAULT NULL,
  `SORT` int(11) NOT NULL,
  `TYPE` varchar(2) NOT NULL COMMENT '1内容，2建设，3结构，4链接，5单页',
  `METATITLE` varchar(256) DEFAULT NULL,
  `METAKEY` varchar(256) DEFAULT NULL,
  `METACONTENT` varchar(256) DEFAULT NULL,
  `LINKURL` varchar(256) DEFAULT NULL,
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `ETIME` varchar(16) NOT NULL,
  `CUSERNAME` varchar(64) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `EUSERNAME` varchar(64) NOT NULL,
  `EUSER` varchar(32) NOT NULL,
  `PCONTENT` varchar(128) DEFAULT NULL,
  `PSTATE` varchar(2) NOT NULL,
  `PARENTID` varchar(32) NOT NULL,
  `TREECODE` varchar(256) NOT NULL DEFAULT '',
  `TAGS` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of farm_doctype
-- ----------------------------
INSERT INTO `farm_doctype` VALUES ('WCP帮助', null, null, '1', '3', null, null, null, null, '402881f14af310bc014af322996f0006', '20150116222538', '20151016103524', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', null, '1', 'NONE', '402881f14af310bc014af322996f0006', null);
INSERT INTO `farm_doctype` VALUES ('错误信息', null, null, '1', '3', null, null, null, null, '402888304e2d75fe014e3d4f55040006', '20150629111446', '20150629111446', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', null, '1', 'NONE', '402888304e2d75fe014e3d4f55040006', null);
INSERT INTO `farm_doctype` VALUES ('java开发', null, null, '1', '3', null, null, null, null, '402888304e2d75fe014e3d72e891000c', '20150629115337', '20150629115337', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', null, '1', 'NONE', '402888304e2d75fe014e3d72e891000c', null);
INSERT INTO `farm_doctype` VALUES ('框架规范示例', null, null, '1', '3', null, null, null, null, '402888304e2d75fe014e3d733de1000d', '20150629115359', '20150629115359', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', null, '1', '402888304e2d75fe014e3d72e891000c', '402888304e2d75fe014e3d72e891000c402888304e2d75fe014e3d733de1000d', null);
INSERT INTO `farm_doctype` VALUES ('eclipse', null, null, '1', '3', null, null, null, null, '402888304e43c0ef014e4c94cb090006', '20150702102456', '20150917210723', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', null, '1', '402888304e2d75fe014e3d72e891000c', '402888304e2d75fe014e3d72e891000c402888304e43c0ef014e4c94cb090006', null);
INSERT INTO `farm_doctype` VALUES ('UI开发', null, null, '5', '3', null, null, null, null, '402888304e7054a6014e99c9071d0024', '20150717101245', '20150717101245', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', null, '1', 'NONE', '402888304e7054a6014e99c9071d0024', null);
INSERT INTO `farm_doctype` VALUES ('框架开发', null, null, '1', '3', null, null, null, null, '402888304e7054a6014e99c934e10025', '20150717101257', '20150717101257', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', null, '1', '402888304e7054a6014e99c9071d0024', '402888304e7054a6014e99c9071d0024402888304e7054a6014e99c934e10025', null);
INSERT INTO `farm_doctype` VALUES ('javascript', null, null, '2', '3', null, null, null, null, '402888304e7054a6014e9b997bf6002d', '20150717184004', '20150717184004', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', null, '1', '402888304e2d75fe014e3d72e891000c', '402888304e2d75fe014e3d72e891000c402888304e7054a6014e9b997bf6002d', null);
INSERT INTO `farm_doctype` VALUES ('设备', null, null, '6', '3', null, null, null, null, '402888304ef10d46014f1c4030bc0028', '20150811181332', '20150917210557', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', null, '1', '402888304e2d75fe014e3d72e891000c', '402888304e2d75fe014e3d72e891000c402888304ef10d46014f1c4030bc0028', null);
INSERT INTO `farm_doctype` VALUES ('开源知识', null, null, '7', '3', null, null, null, null, '402888304ef10d46014f1c5cb982002d', '20150811184443', '20150811184443', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', null, '1', '402888304e2d75fe014e3d72e891000c', '402888304e2d75fe014e3d72e891000c402888304ef10d46014f1c5cb982002d', null);
INSERT INTO `farm_doctype` VALUES ('服务中间件', null, null, '6', '3', null, null, null, null, '402888304ef10d46014f207299ff00c8', '20150812134705', '20150812134705', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', null, '1', 'NONE', '402888304ef10d46014f207299ff00c8', null);
INSERT INTO `farm_doctype` VALUES ('数据库', null, null, '1', '3', null, null, null, null, '402888304ef10d46014f2072b9cf00c9', '20150812134713', '20150812134713', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', null, '1', '402888304ef10d46014f207299ff00c8', '402888304ef10d46014f207299ff00c8402888304ef10d46014f2072b9cf00c9', null);
INSERT INTO `farm_doctype` VALUES ('新闻', null, null, '7', '3', null, null, null, null, '402888ac4f9cb766014f9cd46813002c', '20150905172650', '20150905172650', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', null, '1', 'NONE', '402888ac4f9cb766014f9cd46813002c', null);
INSERT INTO `farm_doctype` VALUES ('使用手册', null, null, '1', '3', null, null, null, null, '402888ac506e7d9401506e80c7de0009', '20151016103544', '20151016103544', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', null, '1', '402881f14af310bc014af322996f0006', '402881f14af310bc014af322996f0006402888ac506e7d9401506e80c7de0009', null);
INSERT INTO `farm_doctype` VALUES ('常见问题', null, null, '2', '3', null, null, null, null, '402888ac506e7d9401506e816562000a', '20151016103625', '20151016103625', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', null, '1', '402881f14af310bc014af322996f0006', '402881f14af310bc014af322996f0006402888ac506e7d9401506e816562000a', null);
INSERT INTO `farm_doctype` VALUES ('开发说明', null, null, '3', '3', null, null, null, null, '402888ac506e7d9401506e81cf15000b', '20151016103652', '20151016103652', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', null, '1', '402881f14af310bc014af322996f0006', '402881f14af310bc014af322996f0006402888ac506e7d9401506e81cf15000b', null);

-- ----------------------------
-- Table structure for `farm_qz_scheduler`
-- ----------------------------
DROP TABLE IF EXISTS `farm_qz_scheduler`;
CREATE TABLE `farm_qz_scheduler` (
  `ID` varchar(32) NOT NULL,
  `AUTOIS` varchar(2) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `ETIME` varchar(16) NOT NULL,
  `CUSERNAME` varchar(64) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `EUSERNAME` varchar(64) NOT NULL,
  `EUSER` varchar(32) NOT NULL,
  `PSTATE` varchar(2) NOT NULL,
  `PCONTENT` varchar(128) DEFAULT NULL,
  `TASKID` varchar(32) NOT NULL,
  `TRIGGERID` varchar(32) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_Reference_18` (`TASKID`),
  KEY `FK_Reference_19` (`TRIGGERID`),
  CONSTRAINT `FK_Reference_18` FOREIGN KEY (`TASKID`) REFERENCES `farm_qz_task` (`ID`),
  CONSTRAINT `FK_Reference_19` FOREIGN KEY (`TRIGGERID`) REFERENCES `farm_qz_trigger` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of farm_qz_scheduler
-- ----------------------------
INSERT INTO `farm_qz_scheduler` VALUES ('402894ca4af0883a014af092c21c0005', '1', '20150116102917', '20150116102917', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '1', null, '402894ca4af0883a014af092863c0004', '402894ca4af0883a014af08d09f00003');

-- ----------------------------
-- Table structure for `farm_qz_task`
-- ----------------------------
DROP TABLE IF EXISTS `farm_qz_task`;
CREATE TABLE `farm_qz_task` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `ETIME` varchar(16) NOT NULL,
  `CUSERNAME` varchar(64) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `EUSERNAME` varchar(64) NOT NULL,
  `EUSER` varchar(32) NOT NULL,
  `PSTATE` varchar(2) NOT NULL,
  `PCONTENT` varchar(128) DEFAULT NULL,
  `JOBCLASS` varchar(128) NOT NULL,
  `NAME` varchar(128) NOT NULL,
  `JOBPARAS` varchar(1024) DEFAULT NULL,
  `JOBKEY` varchar(128) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of farm_qz_task
-- ----------------------------
INSERT INTO `farm_qz_task` VALUES ('402894ca4af0883a014af092863c0004', '20150116102902', '20150116102902', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '1', '', 'com.farm.doc.quartz.task.RecountDocHotnumTask', '计算文章热度', null, 'NONE');

-- ----------------------------
-- Table structure for `farm_qz_trigger`
-- ----------------------------
DROP TABLE IF EXISTS `farm_qz_trigger`;
CREATE TABLE `farm_qz_trigger` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `ETIME` varchar(16) NOT NULL,
  `CUSERNAME` varchar(64) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `EUSERNAME` varchar(64) NOT NULL,
  `EUSER` varchar(32) NOT NULL,
  `PSTATE` varchar(2) NOT NULL,
  `PCONTENT` varchar(128) DEFAULT NULL,
  `DESCRIPT` varchar(128) NOT NULL,
  `NAME` varchar(128) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of farm_qz_trigger
-- ----------------------------
INSERT INTO `farm_qz_trigger` VALUES ('402894ca4af0883a014af08d09f00003', '20150116102302', '20150116102302', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '1', '每年每月每日每时0分0秒', '0 0 * * * ? *', '每小时执行一次');

-- ----------------------------
-- Table structure for `farm_rf_doctextfile`
-- ----------------------------
DROP TABLE IF EXISTS `farm_rf_doctextfile`;
CREATE TABLE `farm_rf_doctextfile` (
  `ID` varchar(32) NOT NULL,
  `FILEID` varchar(32) NOT NULL,
  `DOCID` varchar(32) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_Reference_12` (`DOCID`),
  CONSTRAINT `FK_Reference_12` FOREIGN KEY (`DOCID`) REFERENCES `farm_doc` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of farm_rf_doctextfile
-- ----------------------------

-- ----------------------------
-- Table structure for `farm_rf_doctype`
-- ----------------------------
DROP TABLE IF EXISTS `farm_rf_doctype`;
CREATE TABLE `farm_rf_doctype` (
  `ID` varchar(32) NOT NULL,
  `TYPEID` varchar(32) NOT NULL,
  `DOCID` varchar(32) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_Reference_14` (`TYPEID`),
  KEY `FK_Reference_15` (`DOCID`),
  CONSTRAINT `FK_Reference_14` FOREIGN KEY (`TYPEID`) REFERENCES `farm_doctype` (`ID`),
  CONSTRAINT `FK_Reference_15` FOREIGN KEY (`DOCID`) REFERENCES `farm_doc` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of farm_rf_doctype
-- ----------------------------
INSERT INTO `farm_rf_doctype` VALUES ('402888ac506eaf5b01506ebfe7ab000c', '402881f14af310bc014af322996f0006', '402888ac506eaf5b01506ebfe7aa000b');
INSERT INTO `farm_rf_doctype` VALUES ('402888ac506f5b7401506f8fb8870022', '402881f14af310bc014af322996f0006', '402888ac506f5b7401506f8fb8860021');
INSERT INTO `farm_rf_doctype` VALUES ('402888ac506f5b7401506f9724c70041', '402888ac4f9cb766014f9cd46813002c', '402888ac506f5b7401506f9724c70040');

-- ----------------------------
-- Table structure for `farm_top`
-- ----------------------------
DROP TABLE IF EXISTS `farm_top`;
CREATE TABLE `farm_top` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `CUSERNAME` varchar(64) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `PSTATE` varchar(2) NOT NULL,
  `PCONTENT` varchar(128) DEFAULT NULL,
  `DOCID` varchar(32) NOT NULL,
  `SORT` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_Reference_61` (`DOCID`),
  CONSTRAINT `FK_Reference_61` FOREIGN KEY (`DOCID`) REFERENCES `farm_doc` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of farm_top
-- ----------------------------
INSERT INTO `farm_top` VALUES ('402888ac506eaf5b01506ec0f936000e', '20151016114551', '系统管理员', '40288b854a329988014a329a12f30002', '1', '', '402888ac506eaf5b01506ebfe7aa000b', '1');
INSERT INTO `farm_top` VALUES ('402888ac506f5b7401506f90bfb30027', '20151016153248', '系统管理员', '40288b854a329988014a329a12f30002', '1', '', '402888ac506f5b7401506f8fb8860021', '2');

-- ----------------------------
-- Table structure for `farm_weburl`
-- ----------------------------
DROP TABLE IF EXISTS `farm_weburl`;
CREATE TABLE `farm_weburl` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `ETIME` varchar(16) NOT NULL,
  `CUSERNAME` varchar(64) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `EUSERNAME` varchar(64) NOT NULL,
  `EUSER` varchar(32) NOT NULL,
  `PSTATE` varchar(2) NOT NULL,
  `PCONTENT` varchar(128) DEFAULT NULL,
  `URL` varchar(512) NOT NULL,
  `WEBNAME` varchar(64) NOT NULL,
  `SORT` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of farm_weburl
-- ----------------------------
INSERT INTO `farm_weburl` VALUES ('402888a8504c496e01504c4e27b5000b', '20151009191321', '201510101829', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '1', null, 'www.vaikan.com', '外刊评论', '5');
INSERT INTO `farm_weburl` VALUES ('402888a8504c496e01504c5065c6000c', '20151009191548', '20151009191548', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '1', null, 'www.csdn.net', 'CSDN', '2');
