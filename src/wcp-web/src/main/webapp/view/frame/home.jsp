<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE html>
<html>
<head>
<title><PF:ParameterValue key="config.sys.title" /></title>
<jsp:include page="/view/conf/include.jsp"></jsp:include>
</head>
<body class="easyui-layout">
	<div
		style="text-align: right; color: #000000; background: #fff; padding: 20px; height: 100%;">
		<div
			style="font-size: 12px; margin-top:100px; color: gray; text-align: center; padding-top: 4px;">
			<img src="text/img/logo.png" />
			<h1 style="font-size: 36px;" id="homeTitleId"></h1>
			<h1></h1>
		</div>
	</div>
</body>
</html>