<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<c:if test="${RELATIONDOCS != null }">
<!-- 查询和文档内容相关的知识列表，传入文档id -->
<div class="row doc_column_box">
	<div class="col-sm-12">
		<span class="glyphicon glyphicon-resize-small column_title">相关知识</span>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<hr class="hr_split" />
		<div id="luceneResultMinId">
			<ol>
				<c:forEach items="${RELATIONDOCS}" var="node">
					<li><a href="webdoc/view/Pub${node.docid}.html">${node.title}</a>
					</li>
				</c:forEach>
			</ol>
		</div>
	</div>
</div>
</c:if>